<?php if ($module_header) { ?>
<h3><?php echo $module_header; ?></h3>
<?php }?>
<div class="row">
    <?php foreach ($reviews as $review) { ?>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="product-thumb transition">
            <div class="image"><a href="<?php echo $review['prod_href']; ?>"><img src="<?php echo $review['prod_thumb']; ?>" alt="<?php echo $review['prod_name']; ?>" title="<?php echo $review['prod_name']; ?>" class="img-responsive" /></a></div>
            <div class="caption">
                <h4><a href="<?php echo $review['prod_href']; ?>"><?php echo $review['prod_name']; ?></a></h4>
                <p><?php echo $review['description']; ?></p>
                <?php if ($review['rating']) { ?>
                <div class="rating">
                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                    <?php if ($review['rating'] < $i) { ?>
                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                    <?php } else { ?>
                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                    <?php } ?>
                    <?php } ?>
                </div>
                <?php } ?>
                <p class="price">
                    <?php echo $review['author']; ?>
                    <span class="price-tax"><?php echo $review['date_added']; ?></span>
                </p>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<?php if ($show_all_button) { ?>
<div class="row text-right">
    <a href="<?php echo $link_all_reviews; ?>"><?php echo $text_all_reviews ?></a>
</div>
<?php } ?>
