<?php if(($config_on_off_contact_right =='1') && ($img_left_callback != '')) { ?>
	<?php $class_dialog = 'popup-sm-750';?>
<?php } elseif (($config_on_off_contact_right =='') && ($img_left_callback != '')) { ?>
	<?php $class_dialog = 'popup-sm-550';?>
<?php } elseif(($config_on_off_contact_right =='1') && ($img_left_callback == '')) { ?>
	<?php $class_dialog = 'popup-sm-550';?>
<?php } else { ?>
	<?php $class_dialog = 'popup-sm-450';?>
<?php } ?>
<div id="popup-callback" class="<?php echo $class_dialog;?>">
	<div class="popup-heading"><?php echo isset($config_title_callback_sendthis[$lang_id]) ? $config_title_callback_sendthis[$lang_id]['config_title_callback_sendthis'] : ''; ?></div>
	<div class="popup-center">
	<form id="callback_data" enctype="multipart/form-data" method="post">
		<?php if ($img_left_callback != '') { ?>
		<div class="col-sm-4 hidden-xs">			
			<div class="text-center"><img class="img-phone-contact" src="<?php echo $img_left_callback; ?>"/></div>			
		</div>
		<?php } ?>
		<?php if(($config_on_off_contact_right =='1') && ($img_left_callback != '')) { ?>
			<?php $class_c = 'col-sm-8 col-md-4';?>
		<?php } elseif (($config_on_off_contact_right =='') && ($img_left_callback != '')) { ?>
			<?php $class_c = 'col-sm-8 col-md-8';?>
		<?php } elseif(($config_on_off_contact_right =='1') && ($img_left_callback == '')) { ?>
			<?php $class_c = 'col-sm-6 col-md-6';?>
		<?php } else { ?>
			<?php $class_c = 'col-sm-12';?>
		<?php } ?>
		<div class="<?php echo $class_c;?>">
			<?php if($config_on_off_fields_firstname_cb == '1') { ?>
				<div class="marb <?php echo $config_fields_firstname_requared_cb == '1' ? 'sections_block_rquaired' : 'sections_block' ; ?>">
					<div class="input-group margin-bottom-sm">			
						 <input id="contact-name" class="form-control contact-name" type="text" placeholder="<?php echo $config_placeholder_fields_firstname_cb[$lang_id]['config_placeholder_fields_firstname_cb']; ?>" value="" name="name">		
						<span class="input-group-addon"><i class="icon-append-1 fa fa-user fa-fw"></i></span>
					</div>
					<div id="error_name_callback" class="error_callback"></div>
				</div>
			<?php } ?>
			<?php if($config_on_off_fields_phone_cb == '1') { ?>
				<div class="marb <?php echo $config_fields_phone_requared_cb == '1' ? 'sections_block_rquaired' : 'sections_block' ; ?>">
					<div class="input-group margin-bottom-sm">			
						 <input id="contact-phone" class="form-control contact-phone" type="text" placeholder="<?php echo $config_placeholder_fields_phone_cb[$lang_id]['config_placeholder_fields_phone_cb']; ?>" value="" name="phone">		
						<span class="input-group-addon"><i class="icon-append-1 fa fa-phone-square fa-fw"></i></span>
					</div>
					<div id="error_phone_callback" class="error_callback"></div>
				</div>
			<?php } ?>
			<?php if($config_on_off_fields_email_cb == '1') { ?>
				<div class="marb <?php echo $config_fields_email_requared_cb == '1' ? 'sections_block_rquaired' : 'sections_block' ; ?>">
					<div class="input-group margin-bottom-sm">			
						 <input id="contact-email" class="form-control contact-email" type="text" placeholder="<?php echo $config_placeholder_fields_email_cb[$lang_id]['config_placeholder_fields_email_cb']; ?>" value="" name="email_buyer">		
						<span class="input-group-addon"><i class="icon-append-1 fa fa-envelope fa-fw"></i></span>
					</div>
					<div id="error_email_callback" class="error_callback"></div>
				</div>
			<?php } ?>
			<?php if($config_on_off_fields_comment_cb == '1') { ?>
				<div class="marb <?php echo $config_fields_comment_requared_cb == '1' ? 'sections_block_rquaired' : 'sections_block' ; ?>">
					<div class="input-group margin-bottom-sm">			
						 <input id="contact-comment" class="form-control contact-comment" type="text" placeholder="<?php echo $config_placeholder_fields_comment_cb[$lang_id]['config_placeholder_fields_comment_cb']; ?>" value="" name="comment_buyer">		
						<span class="input-group-addon"><i class="icon-append-1 fa fa-comment fa-fw"></i></span>
					</div>
					<div id="error_comment_callback" class="error_callback"></div>
				</div>
			<?php } ?>
			<?php if($config_on_off_date_time == 1) { ?>
				<div class="marb">
					<div><?php echo $when_you_call_back;?></div>
				</div>				
				<div class="marb">
					<input type="text" name="time_callback_on" value="" class="form-control  time_callback start" placeholder="2015-05-25 15:30" size="14" />
				</div>
				<div class="marb">	
					<input type="text" name="time_callback_off" value="" class="form-control  time_callback end" placeholder="2015-05-25 18:30" size="14" />
				</div>						  
			<?php } ?>
			 <?php if(!empty($call_topic)) {?>
			   <div class="marb">
					<select class="form-control" name="topic_callback_send">
						<option><?php echo $text_topic_callback;?></option>
						<?php foreach($call_topic as $res_call_topic) { ?>
							<option value="<?php echo $res_call_topic['name']?>"><?php echo $res_call_topic['name']?></option>
						<?php } ?>
					</select>	
			   </div>
			   <?php } ?>
			   
		<input type="hidden" id="callback_url" value="" name="url_site"  />	
		</div>
		<?php if(($config_on_off_contact_right =='1') && ($img_left_callback != '')) { ?>
			<?php $class_contact = 'col-sm-4 col-md-4';?>
		<?php } elseif (($config_on_off_contact_right =='') && ($img_left_callback != '')) { ?>
			<?php $class_contact = 'col-sm-4 col-md-4';?>
		<?php } elseif(($config_on_off_contact_right =='1') && ($img_left_callback == '')) { ?>
			<?php $class_contact = 'col-sm-6 col-md-6';?>
		<?php } else { ?>
			<?php $class_contact = 'col-sm-6';?>
		<?php } ?>
		<?php if($config_on_off_contact_right =='1') {?>
		<div class="<?php echo $class_contact;?> hidden-xs">
			<div class="panel panel-default">
				<div class="panel-heading-callback">
					<h3 class="panel-title"><?php echo $block_name_phone[$lang_id]['block_name_phone']; ?></h3>
				</div>
				<div class="panel-body">
					<div class="telephone">
						<?php foreach ($config_telephones as $config_telephone) { ?>
							<div><a href="tel:<?php echo $config_telephone['phone'];?>"><img src="<?php echo $config_telephone['thumb'];?>" />&nbsp;&nbsp;<?php echo $config_telephone['phone'];?></a></div>
						<?php } ?>
					</div>
					<div class="col-sm-12 line_cb"></div>
					<?php if ($config_skype !='') { ?>
					<div class="skype">
					<i class="fa fa-skype fa-fw"></i><?php echo $config_skype; ?>
					</div>
					<div>
					<?php if($delete_status_skype_from_site =='1'){?>
					<?php $timenow = date("H:i");?>
					
					<?php if(($timenow > $time_start_skype) && ($timenow < $time_end_skype)) { ?>
						<a class="status_skype" href="skype:?chat"><img src="catalog/view/theme/default/image/classic_transparent_online.png"></a>
					<?php } else { ?>
						<a class="status_skype" href="skype:?chat"><img src="catalog/view/theme/default/image/classic_transparent_offline.png"></a>
					<?php } ?>
					<?php } ?>
					</div>
					<?php } ?>
					<?php if ($config_email_1 !='') { ?>
						<div class="email">
							<div><i class="fa fa-envelope fa-fw"></i><?php echo $config_email_1; ?></div>					
						</div>					
					<?php } ?>
					<div class="schedule">				
						<div class="title-title-schedule"><?php echo isset($config_title_schedule[$lang_id]) ? $config_title_schedule[$lang_id]['config_title_schedule'] : ''; ?></div>
							<div class="config_daily">
								<?php echo isset($config_daily[$lang_id]) ? $config_daily[$lang_id]['config_daily'] : ''; ?>
							</div>
							<div class="config_weekend">
								<?php echo isset($config_weekend[$lang_id]) ? $config_weekend[$lang_id]['config_weekend'] : ''; ?>
							</div>
					</div>
					
					<?php if ($social_icons_contact) { ?>
						<div class="col-sm-12 line_cb"></div>
						<div class="title-social"><?php echo isset($config_social_block_title[$lang_id]) ? $config_social_block_title[$lang_id]['config_social_block_title'] : ''; ?></div>
						<div class="social">
							<?php foreach($social_icons_contact as $social_icon) { ?>
								<span><a href="http://<?php echo $social_icon['name']; ?>" target="_blank"><span class="social_icon"><img src="<?php echo $social_icon['thumb']; ?>"/></span></a></span>
							<?php } ?>
						</div>
					<?php } ?>
				</div>			
			</div>
		</div>
		<?php } ?>
		<div class="marb col-sm-12 text-center"><?php echo isset($config_any_text_bottom_before_button[$lang_id]) ? $config_any_text_bottom_before_button[$lang_id]['config_any_text_bottom_before_button'] : ''; ?></div>
	</form>
	</div>
	<div class="popup-footer">
		<?php global $config;?>
		<style type="text/css">
			.btn-callback {
				background-color:#<?php echo $config->get('config_background_button_callback') ;?> !important;
				border-color:#<?php echo $config->get('config_background_button_callback') ;?> !important;
			}
			.btn-callback:hover, .btn-callback:active, .btn-callback:focus {
				background-color:#<?php echo $config->get('config_background_button_callback_hover') ;?> !important; 
				border-color:#<?php echo $config->get('config_background_button_callback_hover') ;?> !important; 
			}
		</style>
		<div class="col-sm-12 text-center"><button onclick="sendCallback();" type="button" class="btn-callback"><i class="fa fa-phone-square fa-fw"></i><span class="spinner"><i class="fa fa-spinner fa-pulse fa-fw"></i></span><?php echo $button_send; ?></button></div>
	</div>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css"/> 
<script type="text/javascript" src="catalog/view/javascript/jquery/datetimepicker/moment.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
	$('.start').datetimepicker({
		pickDate: true,
		pickTime: true
	});
	$('.end').datetimepicker({
		pickDate: true,
		pickTime: true
	});
</script>
<script type="text/javascript">
$(document).ready(function() {
    $("#contact-phone").mask('<?php echo $config_mask_phone_number_cb;?>');
});
</script>
</div>