$(document).ready(function() {
	$('.cat-recommendations .cat-recommendations-tabs li').click(function(e){
		e.preventDefault();
		var tab = $(this).data('cat-recommendations');
		
		$('.cat-recommendations .cat-recommendations-tabs li').removeClass('active');
		$(this).addClass('active');
		
		$('.cat-recommendations .box-tab-content').removeClass('active');
		$('.cat-recommendations .box-tab-content[data-cat-recommendations="' +tab+ '"]').addClass('active');	
	});	
});