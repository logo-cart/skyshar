/* Hi-Optimizer (c) owl.carousel optimizer by sitecreator (c) 2019-2020 https://sitecreator.ru owl.carousel.js */
/* заменяем ссылку на owl.carousel.js */
(function(w, d) {
  w.fakeOwlCarouselExistsHiOptim = true;
  w.owlCarouselHiOptim = {};
  w.owlCarouselHiOptim.setting = [];
  w.owlCarouselHiOptim.obj = [];

})(window, document);


document.addEventListener("DOMContentLoaded", function() {
  var exists = function(w) {
    // дождемся инициализации настоящей
    if(typeof jQuery.fn.owlCarousel === 'function' && typeof jQuery.fn.owlCarousel.options === 'object') {
      console.log('owlCarousel exists');
      var l = w.owlCarouselHiOptim.obj.length;
      for (var i=0; i < l; i++) {
        w.owlCarouselHiOptim.obj[i].owlCarousel(w.owlCarouselHiOptim.setting[i]);
      }
    }
    else setTimeout(exists, 20, window); // ждем инициализации настоящей owlCarousel
  };
  var started = function (d) {
    var startT = new Date().getTime();
    var js = d.createElement("script");
    js.src = "_src_";
    // if ((d.readyState === "complete" || d.readyState === "interactive" || (new Date().getTime() - startT) > 3000) && typeof jQuery === 'function') {
    if (typeof jQuery === 'function') {
      var el = d.getElementsByTagName('script')[0];
      // дождемся инициализации настоящей
      el.parentNode.insertBefore(js, el);
      console.log('owl.carousel(.min).js started (low priority): ' + js.src);
      setTimeout(exists, 0, window);
    } else setTimeout(started, 20, document);
  };
    setTimeout(started, 0, document);
});