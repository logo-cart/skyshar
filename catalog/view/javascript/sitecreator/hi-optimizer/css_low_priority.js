/* Hi-Optimizer (c) 2019-2020 https://sitecreator.ru css_low_priority.js */
if(typeof window.hi_optimizer_fun_low_priority !== 'function') {
  function hi_optimizer_fun_low_priority(href) {
    document.addEventListener("DOMContentLoaded", function() {
      var t = new Date().getTime();
      var css = document.createElement( "link" );
      css.rel = "preload";
      css.as = "style";
      css.href = href;
      document.head.appendChild(css);
      console.log('css load start (low priority): ' + href);
      var f = function (css, t) {
      if(document.readyState === "complete" || (new Date().getTime() - t) > 10000) {
        css.rel = "stylesheet";
        console.log('css started (low priority): ' + css.href);
      }
      else setTimeout(f, 50, css, t);
    };
    setTimeout(f, 0, css, t);
    });
  }
}