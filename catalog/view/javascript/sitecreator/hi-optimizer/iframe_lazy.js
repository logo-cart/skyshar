
// Hi-Optimizer (c) Lazy Load for iframe by sitecreator (c) 2019-2020 https://sitecreator.ru iframe_lazy.js
function LazyLoadIframeStcrtr() {

  var busy = false, observer, config = {root: null, rootMargin: '0px', threshold: [0.2]};

  if (typeof window.IntersectionObserver === 'function') {
    observer = new IntersectionObserver(function (entries, self) {
      // еще до пересечения будут перебраны все записи (элементы, за которыми наблюдаем) один раз
      Array.prototype.forEach.call(entries, function (e) {
        if (e.isIntersecting) {
          self.unobserve(e.target);
          setSrc(e.target);
        }
      });
    }, config);
  }

  this.lazyReStart = function () {
    if (busy) return;
    busy = true;
    var els = document.querySelectorAll("iframe[data-src]");
    Array.prototype.forEach.call(els, function (el) {
      if(typeof observer === 'object') observer.observe(el);
      else setSrc(el);
    });
    busy = false;
  };

  // приватная ф-я
  function setSrc(el) {
    var src = el.getAttribute('data-src');
    if (src) {
      el.src = src;
      el.removeAttribute('data-src');
    }
  }
}

(function() {
  var lazy = new LazyLoadIframeStcrtr();
  //lazy.lazyReStart();
  setInterval(function () {lazy.lazyReStart();}, 100);
})();
