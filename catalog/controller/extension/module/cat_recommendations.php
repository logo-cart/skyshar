<?php
	class ControllerExtensionModuleCatRecommendations extends Controller {
		public function index($setting) {
			static $module = 0;
			
			$this->load->language('extension/module/cat_recommendations');
			
			$this->load->model('catalog/product');
			
			$this->load->model('tool/image');
			
			$this->document->addStyle('catalog/view/javascript/jquery/swiper/css/swiper.min.css');
			$this->document->addStyle('catalog/view/javascript/jquery/swiper/css/opencart.css');
			$this->document->addScript('catalog/view/javascript/jquery/swiper/js/swiper.jquery.js');
			$this->document->addScript('catalog/view/javascript/cat-recommendations.js');
			$this->document->addStyle('catalog/view/theme/default/stylesheet/cat-recommendations.css');	
			
			$this->load->model('catalog/category');
			$categories = $this->model_catalog_category->catRecomGetCategories(0);
			
			$data['category_list'] = array();
			
			foreach ($categories as $category) {
				$cat_recommendations = $setting['cat_recommendations_product_' . $category['category_id']];
				$products = explode(',', $cat_recommendations);
				
				if ($cat_recommendations && $products){
					$products_list = array();
					
					foreach ($products as $product_id) {
						$product_info = $this->model_catalog_product->getProduct($product_id);
						
						if ($product_info) {
							if ($product_info['image']) {
								$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
								} else {
								$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
							}
							
							if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
								$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
								} else {
								$price = false;
							}
							
							if ((float)$product_info['special']) {
								$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
								} else {
								$special = false;
							}
							
							if ($this->config->get('config_tax')) {
								$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
								} else {
								$tax = false;
							}
							
							if ($this->config->get('config_review_status')) {
								$rating = $product_info['rating'];
								} else {
								$rating = false;
							}
							
							$products_list[] = array(
							'product_id'  => $product_info['product_id'],
							'thumb'       => $image,
							'name'        => $product_info['name'],
							'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
							'price'       => $price,
							'special'     => $special,
							'tax'         => $tax,
							'rating'      => $rating,
							'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
							);
						}
						
					}
					
					$data['category_list'][] = array(
					'cat_recommendations_product' => $this->config->get('cat_recommendations_product_' . $category['category_id']),
					'category_id' => $category['category_id'],
					'name'       => $category['name'],
					'products'   => $products_list
					);
					
				}
				
			}
			
			$data['module'] = $module++;
			
			if ($data['category_list']) {
				return $this->load->view('extension/module/cat_recommendations', $data);
			}
		}
	}							