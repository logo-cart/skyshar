<?php
class ControllerExtensionModuleafoc3 extends Controller {
	public function index($setting) {


		$this->load->language('basel/basel_theme');	
		$this->load->language('extension/module/afoc3');
		
		$this->load->model('extension/module/afoc3');
		$this->load->model('catalog/product');
				
		$data['heading_title'] = $setting['title'][$this->config->get('config_language_id')]['name'];

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');


		$data['tabstyle'] = $setting['tabstyle'];
		$data['carousel'] = $setting['carousel'];
		$data['carousel_a'] = $setting['carousel_a'];
		$data['carousel_b'] = $setting['carousel_b'];
		$data['columns'] = $setting['columns'];
		$data['rows'] = $setting['rows'];
		$data['use_margin'] = $setting['use_margin'];
		$data['margin'] = $setting['margin'];
		$data['img_width'] = $setting['image_width'];
		$data['use_button'] = $setting['use_button'];
		$data['link_href'] = $setting['link_href'];
		$data['countdown_status'] = $setting['countdown'];	
		$data['basel_list_style'] = $this->config->get('basel_list_style');
		$data['stock_badge_status'] = $this->config->get('stock_badge_status');
		$data['basel_text_out_of_stock'] = $this->language->get('basel_text_out_of_stock');
		$data['default_button_cart'] = $this->language->get('button_cart');
		$data['salebadge_status'] = $this->config->get('salebadge_status');
		
		$this->load->model('extension/module/afoc3');
		
		$products_src = array();
		
		if (isset($this->request->get['product_id'])) {
			$products_src[] = $this->request->get['product_id'];
		}
		$products = $this->model_extension_module_afoc3->getProducts($setting, $products_src);  
		
		$this->load->model('tool/image');

		$data['products'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}

		$data['columns'] = 4;

		if ($products) {
			foreach ($products as $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);

				if ($product_info) {
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}

					$images = $this->model_catalog_product->getProductImages($product_info['product_id']);
					if(isset($images[0]['image']) && !empty($images[0]['image'])){
					$images =$images[0]['image'];
				   	} else {
					$images = false;
					}

					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$price = false;
					}

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}

					$image2 = $this->model_catalog_product->getProductImages($product_info['product_id']);
					if(isset($image2[0]['image']) && !empty($image2[0]['image']) && $this->config->get('basel_thumb_swap')){
						$image2 = $image2[0]['image'];
					} else {
						$image2 = false;
					}

					if (strtotime($product_info['date_available']) > strtotime('-' . $this->config->get('newlabel_status') . ' day')) {
						$is_new = true;
					} else {
						$is_new = false;
					}
					if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}	
					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}

					$options = array();
				
					foreach ($this->model_catalog_product->getProductOptions($product_info['product_id']) as $option) {
						$product_option_value_data = array();

						foreach ($option['product_option_value'] as $option_value) {
							if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
								
								if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
									$options_price = $option_value['price'];
								} else {
									$options_price = false;
								}

								$product_option_value_data[] = array(
									'product_option_value_id' => $option_value['product_option_value_id'],
									'option_value_id'         => $option_value['option_value_id'],
									'name'                    => $option_value['name'],
									'image'                   => $this->model_tool_image->resize($option_value['image'], 50, 50),
									'price'                   => $options_price,
									'price_prefix'            => $option_value['price_prefix']
								);
							}
						}

						$options[] = array(
							'product_option_id'    => $option['product_option_id'],
							'product_option_value' => $product_option_value_data,
							'option_id'            => $option['option_id'],
							'name'                 => $option['name'],
							'type'                 => $option['type'],
							'value'                => $option['value'],
							'required'             => $option['required']
						);
					}

					$data['products'][] = array(
						'product_id'  => $product_info['product_id'],
						'quantity'  => $product_info['quantity'],
						'options'       => $options,
						'thumb'       => $image,
						'thumb2' 	 => $this->model_tool_image->resize($image2, $setting['width'], $setting['height']),
						'name'        => $product_info['name'],
						'new_label'  => $is_new,
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
						'price'       => $price,
						'special'     => $special,
						'tax'         => $tax,
						'rating'      => $rating,
						'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					);
				}
			}
		}

		$data['module'] = 11;

		if ($data['products']) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_theme') . '/template/extension/module/afoc3.twig')) {
				return $this->load->view('extension/module/afoc3', $data);
			} elseif (file_exists(DIR_TEMPLATE . $this->config->get('config_theme') . '/template/extension/module/featured.twig')) {
				return $this->load->view('extension/module/featured', $data);
			} else {
				return $this->load->view('extension/module/afoc3', $data);
			}
		}
			
	}}