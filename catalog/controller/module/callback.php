<?php 
class ControllerModuleCallback extends Controller { 
	private $error = array();
 
	public function index() {
	
	$this->load->model('module/callback');	
		$this->load->language('module/callback');	
		$json = array();
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['action'])) {
			if ($this->validate()) {
				$data = array();
				if (isset($this->request->post['name'])) {
  		    			$data['name'] = $this->request->post['name'];
				} else {
      					$data['name'] = '';
    				}
				if (isset($this->request->post['phone'])) {
      					$data['phone'] = $this->request->post['phone'];
				} else {
      					$data['phone'] = '';
    				}
				if (isset($this->request->post['comment_buyer'])) {
      					$data['comment_buyer'] = $this->request->post['comment_buyer'];
				} else {
      					$data['comment_buyer'] = '';
    				}
				if (isset($this->request->post['email_buyer'])) {
      					$data['email_buyer'] = $this->request->post['email_buyer'];
				} else {
      					$data['email_buyer'] = '';
    				}
				if (isset($this->request->post['date_callback'])) {
      					$data['date_callback'] = $this->request->post['date_callback'];
				} else {
      					$data['date_callback'] = '';
    				}
				if (isset($this->request->post['time_callback_on'])) {
      					$data['time_callback_on'] = $this->request->post['time_callback_on'];
				} else {
      					$data['time_callback_on'] = '';
    				}
				if (isset($this->request->post['time_callback_off'])) {
      					$data['time_callback_off'] = $this->request->post['time_callback_off'];
				} else {
      					$data['time_callback_off'] = '';
    				}
				if (isset($this->request->post['url_site'])) {
                          $data['url_site'] = $this->request->post['url_site'];
                } else {
                          $data['url_site'] = '';
                    }
				if (isset($this->request->post['topic_callback_send'])) {
                          $data['topic_callback_send'] = $this->request->post['topic_callback_send'];
                } else {
                          $data['topic_callback_send'] = '';
                    }
				$data['store_name'] = $this->config->get('config_name');
				$data['language_id'] = $this->config->get('config_language_id');				
				$this->load->model('module/callback');
				$results = $this->model_module_callback->addCallback($data);
				$config_on_off_send_me_mail_callback = $this->config->get('config_on_off_send_me_mail_callback');
				$config_you_email_callback = $this->config->get('config_you_email_callback');
					if($config_on_off_send_me_mail_callback =='1'){
						if($config_you_email_callback != ''){
							$this->sendMail($data);	
						}					
					}
					if($this->config->get('config_send_sms_on_off') == '1'){
						$this->sendSms($data);	
					}
				$json['success'] = $this->language->get('ok');
			}else{
				$json['warning'] = $this->error;
			}
			
			return $this->response->setOutput(json_encode($json));
		}
		
			$data['sendthis'] = $this->language->get('sendthis');
     		$data['comment_buyer'] = $this->language->get('comment_buyer');
     		$data['email_buyer'] = $this->language->get('email_buyer');
     		$data['namew'] = $this->language->get('namew');
     		$data['phonew'] = $this->language->get('phonew');	
     		$data['button_send'] = $this->language->get('button_send');
     		$data['cancel'] = $this->language->get('cancel');
     		$data['when_you_call_back'] = $this->language->get('when_you_call_back');
     		$data['text_you_comment'] = $this->language->get('text_you_comment');
			$data['footer_desc'] = $this->config->get('footer_description');
			
			$data['lang_id'] 								= $this->config->get('config_language_id');
			$data['mob'] 									= $this->config->get('config_mob');
			$data['mob2'] 									= $this->config->get('config_mob2');
			$data['mob3'] 									= $this->config->get('config_mob3');
			$data['config_email_1'] 						= $this->config->get('config_email_1');
			$data['config_skype'] 							= $this->config->get('config_skype');
			$data['time_start_skype'] 						= $this->config->get('config_skype_date_start');
			$data['time_end_skype'] 						= $this->config->get('config_skype_date_end');
			$data['delete_status_skype_from_site'] 			= $this->config->get('delete_status_skype_from_site');
			$data['block_name_phone'] 						= $this->config->get('block_name_phone');
			$data['config_title_callback_sendthis']			 = $this->config->get('config_title_callback_sendthis');
			$data['config_title_schedule'] 					= $this->config->get('config_title_schedule');
			$data['config_daily'] 							= $this->config->get('config_daily');
			$data['config_weekend'] 						= $this->config->get('config_weekend');		
			$data['config_on_off_date_time'] 				= $this->config->get('config_on_off_date_time');		
			
			
			$data['background_phone1'] 						= $this->config->get('config_background_phone1');		
			$data['background_phone3'] 						= $this->config->get('config_background_phone3');		
			$data['color_email'] 							= $this->config->get('config_color_email');		
			$data['color_skype'] 							= $this->config->get('config_color_skype');		
			$data['color_clock'] 							= $this->config->get('config_color_clock');		
			$data['background_button_callback'] 			= $this->config->get('config_background_button_callback');		
			$data['background_button_callback_hover'] 		= $this->config->get('config_background_button_callback_hover');
			$data['text_topic_callback'] 					= $this->language->get('text_topic_callback');
			$data['social_icons'] 							= $this->model_module_callback->getSocial();
			$data['call_topic'] 							= $this->model_module_callback->getCallbacktopic();
			
			/*NEW SETTING*/
			$data['config_on_off_fields_firstname_cb'] 		= $this->config->get('config_on_off_fields_firstname_cb');
			$data['config_fields_firstname_requared_cb'] 	= $this->config->get('config_fields_firstname_requared_cb');
			$data['config_placeholder_fields_firstname_cb'] = $this->config->get('config_placeholder_fields_firstname_cb');
			
			$data['config_on_off_fields_phone_cb'] 			= $this->config->get('config_on_off_fields_phone_cb');
			$data['config_fields_phone_requared_cb'] 		= $this->config->get('config_fields_phone_requared_cb');
			$data['config_placeholder_fields_phone_cb'] 	= $this->config->get('config_placeholder_fields_phone_cb');
			$data['config_mask_phone_number_cb'] 			= $this->config->get('config_mask_phone_number_cb');
			
			$data['config_on_off_fields_comment_cb'] 		= $this->config->get('config_on_off_fields_comment_cb');
			$data['config_fields_comment_requared_cb'] 		= $this->config->get('config_fields_comment_requared_cb');
			$data['config_placeholder_fields_comment_cb'] 	= $this->config->get('config_placeholder_fields_comment_cb');
			
			$data['config_on_off_fields_email_cb'] 			= $this->config->get('config_on_off_fields_email_cb');
			$data['config_fields_email_requared_cb'] 		= $this->config->get('config_fields_email_requared_cb');
			$data['config_placeholder_fields_email_cb'] 	= $this->config->get('config_placeholder_fields_email_cb');
			$data['config_social_block_title'] 				= $this->config->get('config_social_block_title');
			$data['config_any_text_bottom_before_button'] 	= $this->config->get('config_any_text_bottom_before_button');
			$data['config_on_off_contact_right'] 			= $this->config->get('config_on_off_contact_right');
			
			/*-----------*/
			
			
			
			$this->load->model('tool/image');
			$data['no_image'] = $this->model_tool_image->resize('no_image.jpg', 24, 24);
			$data['social_icons_contact'] = array();
		
			foreach ($data['social_icons'] as $social_icon) {
				if ($social_icon['image'] && file_exists(DIR_IMAGE . $social_icon['image'])) {
					$image = $social_icon['image'];
				} else {
					$image = 'no_image.jpg';
				}
			
				$data['social_icons_contact'][] = array(
					'social_id'      => $social_icon['social_id'],
					'name'      => $social_icon['name'],
					'image'      => $social_icon['image'],
					'thumb'      => $this->model_tool_image->resize($image, 24, 24),
				);
			}
			
			$config_phones = $this->config->get('config_phones');
			$data['config_telephones'] = array();
			if($config_phones){
				foreach ($config_phones as $config_phone) {
					if ($config_phone['image']) {
						$image = $this->model_tool_image->resize($config_phone['image'], 24, 24);
					} else {
						$image = '';
					}
					$data['config_telephones'][] = array(
						'phone'     => $config_phone['phone'],
						'thumb'     => $image,
					);
				}
			}
			
			
			if ($this->request->server['HTTPS']) {
				$server = $this->config->get('config_ssl');
			} else {
				$server = $this->config->get('config_url');
			}
			
			
			$select_design_theme_callback = $this->config->get('select_design_theme_callback');
			if($select_design_theme_callback =='1'){
				if (is_file(DIR_IMAGE . $this->config->get('config_img_left'))) {
					$data['img_left_callback'] = $this->model_tool_image->resize($this->config->get('config_img_left'), 400, 400);
				} else {
					$data['img_left_callback'] = '';
				}
			} else {
				if (is_file(DIR_IMAGE . $this->config->get('config_img_left'))) {
					$data['img_left_callback'] = $this->model_tool_image->resize($this->config->get('config_img_left'), 120, 120);
				} else {
					$data['img_left_callback'] = '';
				}
			}
			
			
			
			
			
			if($select_design_theme_callback =='1'){
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/callback.tpl')) {
					$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/module/callback.tpl', $data));
				} else {
					$this->response->setOutput($this->load->view('default/template/module/callback.tpl', $data));
				}
			} else {
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/callback2.tpl')) {
					$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/module/callback2.tpl', $data));
				} else {
					$this->response->setOutput($this->load->view('default/template/module/callback2.tpl', $data));
				}
			}
			
					
  	}

  	private function validate() {
   		$this->load->language('module/callback');	
			$config_fields_firstname_requared_cb = $this->config->get('config_fields_firstname_requared_cb');
			$config_on_off_fields_firstname_cb = $this->config->get('config_on_off_fields_firstname_cb');
			if(($config_fields_firstname_requared_cb =='1') && $config_on_off_fields_firstname_cb =='1'){
				if ((strlen(utf8_decode($this->request->post['name'])) < 1) || (strlen(utf8_decode($this->request->post['name'])) > 32)) {
					$this->error['name'] = $this->language->get('mister');
				}
			}
			$config_fields_phone_requared_cb = $this->config->get('config_fields_phone_requared_cb');
			$config_on_off_fields_phone_cb = $this->config->get('config_on_off_fields_phone_cb');
			if(($config_fields_phone_requared_cb =='1') && $config_on_off_fields_phone_cb =='1'){
				if ((strlen(utf8_decode($this->request->post['phone'])) < 3) || (strlen(utf8_decode($this->request->post['phone'])) > 32)) {
					$this->error['phone'] = $this->language->get('wrongnumber');
				}
			}
			
			$config_fields_comment_requared_cb = $this->config->get('config_fields_comment_requared_cb');
			$config_on_off_fields_comment_cb = $this->config->get('config_on_off_fields_comment_cb');
			if(($config_fields_comment_requared_cb =='1') && $config_on_off_fields_comment_cb == '1'){
				if ((strlen(utf8_decode($this->request->post['comment_buyer'])) < 1) || (strlen(utf8_decode($this->request->post['comment_buyer'])) > 400)) {
					$this->error['comment_buyer'] = $this->language->get('comment_buyer_error');
				}
			}
			$config_fields_email_requared_cb = $this->config->get('config_fields_email_requared_cb');
			$config_on_off_fields_email_cb = $this->config->get('config_on_off_fields_email_cb');
			if(($config_fields_email_requared_cb =='1') && $config_on_off_fields_email_cb == '1'){
				if(!preg_match("/^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$/", $this->request->post['email_buyer'])){
						$this->error['email_error'] =  $this->language->get('email_buyer_error');
				}
			}
			

    		if (!$this->error) {
     	 		return true;
    		} else {
     			return false;
   	 	}
	}
	private function getCustomFields($order_info, $varabliesd) {
			$instros = explode('~', $varabliesd);
			$instroz = "";
			foreach ($instros as $instro) {
				if ($instro == 'totals' || isset($order_info[$instro]) ){
					if ($instro == 'totals'){
					    $instro_other = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], true);
					}
					if(isset($order_info[$instro])){
						$instro_other = $order_info[$instro];
					}
				}
				else {
				    $instro_other = nl2br(htmlspecialchars_decode($instro));
				}
				    $instroz .=  $instro_other;
			}
			return $instroz;
	}
	private function sendMail($data) {
		$this->language->load('module/callback');
		$text = '';
		$subject_get = $this->getCustomFields($data, $this->config->get('quickorder_subject_me_callback' . $data['language_id']));
		if ((strlen(utf8_decode($subject_get)) > 5)){
			$subject = $subject_get;
		} else {
			$subject = $this->language->get('subject');
		}
		$html = $this->getCustomFields($data, $this->config->get('quickorder_description_me_callback' . $data['language_id'])). "\n";
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setTo($this->config->get('config_you_email_callback'));
		$mail->setFrom($this->config->get('config_you_email_callback'));
		$mail->setSender(html_entity_decode($data['store_name'], ENT_QUOTES, 'UTF-8'));
		$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
		$mail->setHtml(html_entity_decode($html, ENT_QUOTES, 'UTF-8'));
		$mail->setText($text);
		$mail->send();	
	}
	private function sendSms($data) {
		if($this->config->get('config_send_sms_on_off') == '1'){
		include_once('smsc_api.php');
		$tel = $this->config->get('config_phone_number_send_sms');
		list($sms_id, $sms_cnt, $cost, $balance) = send_sms($tel,$data['name']."\n".$data['phone'], 0, 0, 0, 0, false, "maxsms=3");
		}
	}
}
?>
