<?php
// Modules
$_['heading_title_latest'] 	= 	'Latest Blog Posts';
$_['heading_title_category'] = 	'Blog Categories';
$_['text_show_all'] = 			'Visit blog';

// Blog
$_['text_blog'] = 				'Blog';
$_['text_filter_by'] = 			'Blog Posts Tagged As: ';
$_['text_posted_by'] = 			'Posted By';
$_['text_read'] = 				'Times Read';
$_['text_comments'] = 			'Комментарий';
$_['text_related_blog'] = 		'Related Posts';
$_['text_related_products'] = 	'Рекомендуемые';
$_['text_write_comment'] = 		'Оставить комментарий';
$_['text_no_blog_posts'] = 		'No blog posts to list';
$_['text_error'] = 				'Page not found';
$_['text_read_more'] = 			'Read more';
$_['text_tags'] = 				'Tags:';
$_['text_tax'] = 				'Ex Tax:';
$_['text_write_comment'] = 		'Оставить комментарий';
$_['email_notification'] = 		'New blog comment from: %s';

// Months
$_['text_month_jan'] = 			'Jan';
$_['text_month_feb'] = 			'Feb';
$_['text_month_mar'] = 			'Mar';
$_['text_month_apr'] = 			'Apr';
$_['text_month_may'] = 			'May';
$_['text_month_jun'] = 			'Jun';
$_['text_month_jul'] = 			'Jul';
$_['text_month_aug'] = 			'Aug';
$_['text_month_sep'] = 			'Sep';
$_['text_month_oct'] = 			'Oct';
$_['text_month_nov'] = 			'Nov';
$_['text_month_dec'] = 			'Dec';

// Comment
$_['entry_name'] = 				'Имя';
$_['entry_email'] = 			'E-mail';
$_['entry_comment'] = 			'Комментарий';
$_['entry_captcha'] = 			'Анти Бот';
$_['button_send'] = 			'Отправить';
$_['text_success_approve'] = 	'Спасибо за комментарий';
$_['text_success'] = 			'Thank you! Your comment was successfully sent';
$_['error_name'] = 				'Your Name must be between 2 and 64 characters';
$_['error_email'] = 			'Error: Your email address is not valid';
$_['error_comment'] = 			'Error: Comment Text must be between 5 and 3000 characters';
$_['error_captcha'] = 			'Error: запоните все поля отмеченные звездочкой';