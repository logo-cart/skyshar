<?php
// Heading
$_['heading_title']    = '������������� ������ �� ����������';

// Text
$_['text_extension']   = '����������';
$_['text_success']     = '��������� ������� ��������!';
$_['text_edit']        = '��������� ������';

// Entry
$_['entry_name']       = '�������� ������';
$_['entry_product']    = '������';
$_['entry_category']   = '���������';
$_['entry_limit']      = '�����';
$_['entry_width']      = '������';
$_['entry_height']     = '������';
$_['entry_status']     = '������';

// Help
$_['help_product']     = '(��������������)';

// Error
$_['error_permission'] = '� ��� ��� ���� ��� ���������� ������� �������!';
$_['error_name']       = '�������� ������ ������ ��������� �� 3 �� 64 ��������!';
$_['error_width']      = '������� ������ �����������!';
$_['error_height']     = '������� ������ �����������!';

?>