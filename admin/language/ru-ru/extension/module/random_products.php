<?php
// Heading
$_['heading_title']    = 'Случайные товары из определенной категории';

// Text
$_['text_extension']   = 'Модули';
$_['text_success']     = 'Успех: Вы изменили модуль Случайные товары из определенной категории!';
$_['text_edit']        = 'Редактировать модуль Случайные товары из определенной категории';

// Entry
$_['entry_name']       = 'Название модуля';
$_['entry_limit']      = 'Количество случайных товаров';
$_['entry_category']   = 'Выберите категорию';
$_['entry_status']     = 'Статус';
$_['entry_width']      = 'Ширина изображения товара';
$_['entry_height']     = 'Высота изображения товара';

// Error
$_['error_permission'] = 'Внимание: у вас нет прав на изменение модуля Случайные товары из определенной категории!';
$_['error_name']       = 'Название модуля должно быть от 3 до 64 символов!';
$_['error_width']      = 'Ширина требуется!';
$_['error_height']     = 'Требуется высота!';