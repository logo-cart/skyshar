<?php
// Heading
$_['heading_title']    = 'Мета данные стандартных страниц';

// Text
$_['text_extension']   = 'Расширения';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Настройки модуля';

// Entry
$_['tab_contact']      = 'Контакты';
$_['tab_special']      = 'Акции';
$_['tab_manufacturer'] = 'Производители';

// Entry
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'У Вас нет прав для изменения модуля Мета данные стандартных страниц!';

