<?php  
// Heading
$_['heading_title']        												= 'Callback [<a href="http://feofan.net" target="_blank">feofan.net</a>]';
// Text	
$_['text_success']        												= 'Call list updated';
$_['text_id']      	 	 												= '№';
$_['text_name']      	  												= 'name';
$_['text_manager']      	  											= 'manager';
$_['text_manager_form']      											= 'Select manager';
$_['text_telephone']   	  												= 'phone';
$_['text_comment']     			 										= 'Comment Manager';
$_['text_comment_buyer']   	 											= 'Comment Buyer';
$_['text_email_buyer']   	 											= 'E-mail Buyer';
$_['text_status']      	 												= 'Status';
$_['text_added']      	 												= 'Application date';
$_['text_modified']       												= 'Date of change';
$_['text_action']         												= 'Action';
$_['text_edit']       	  												= 'Edit';
$_['text_callback_url']   	 											= 'URL from which the Order call';

$_['status_wait']         												= 'Expectation';
$_['status_done']         												= 'processed';

$_['column_name']         												= 'Name';
$_['column_telephone']    												= 'Phone';
$_['column_date_added']   												= 'Application date';
$_['text_yes']         													= 'Yes';
$_['text_no']         													= 'No';
$_['text_top_callback_position']         								= 'ON OFF Unit in the header:';
$_['text_right_callback_position_fixed']    							= 'On Off unit on the right (fixed):';
$_['text_right_callback_position_fixed_new_btn']    					= 'On Off unit on the right (fixed) Animations Buttons:';
$_['entry_position_callback']    										= 'Select location';
$_['text_position_top_left']    										= 'Left Top Page';
$_['text_position_center_left']    										= 'Left Centre Page';
$_['text_position_bottom_left']    										= 'Left Footer Page';
$_['text_position_top_right']    										= 'Right Top Page';
$_['text_position_center_right']    									= 'Right Center Page';
$_['text_position_bottom_right']    									= 'Right Footer Page';
$_['text_left_callback_position_fixed_new_btn']    						= 'ON OFF BUTTON ANIMATION Button №1:';
$_['text_callback_position_fixed_new_btn2']    							= 'ON OFF BUTTON ANIMATION Button №2:';
$_['text_bottom_callback_position_icon_text']    						= 'Pressed on off module to the bottom of the page:';
$_['text_on_off_date_time']    											= 'On Off date and time of call';
$_['text_position_left_bottom']    										= 'On the left page';
$_['text_position_center_bottom']    									= 'Center page';
$_['text_position_right_bottom']    									= 'right page';
$_['entry_title_callback_sendthis']    									= 'The name of the block Request a call back:';
$_['entry_img_left_callback']    										= 'Add picture to call back:';
$_['entry_instruction_icon_change'] 									= '<span style="color:#727272;">To change the icon on the button, open the site<a href="http://fortawesome.github.io/Font-Awesome/icons/"  target="_blank">http://fortawesome.github.io/Font-Awesome/icons/</a> select an icon, open it and copy class</br> it is like that ->       fa fa-phone-square      <-:</span>';
$_['entry_icon_class'] 													= 'Add an icon in the header and right (icon view):';
$_['entry_title_link'] 													= 'Module name in the header, footer and the right tip:';
$_['entry_icon_link_color'] 											= 'Change the color of icons and text links:';
$_['entry_background_tooltip_callback'] 								= 'Change the background color in the prompt:';
$_['entry_color_tooltip_callback'] 										= 'Change the color of the text in the prompt:';
$_['entry_border_tooltip_callback'] 									= 'Change the color of the frame in the prompt:';
$_['entry_icon_size_callback'] 											= 'Change the size of icons in px:';
$_['entry_title_mob'] 													= 'The name of the auxiliary contacts:';
$_['entry_mob'] 														= 'Phone №1:';
$_['entry_mob2'] 														= 'Phone №2:';
$_['entry_mob3'] 														= 'Phone №3:';
$_['entry_background_phone1'] 											= 'Change the color of icons phone №1 и №2:';
$_['entry_background_phone3'] 											= 'Change the color of icons phone телефон №3:';
$_['entry_email'] 														= 'Email:';
$_['entry_color_email'] 												= 'Change the color of icons Email:';
$_['entry_skype'] 														= 'Skype:';
$_['time_on_status_skype'] 												= 'On Time Status online(SKYPE)';
$_['time_off_status_skype'] 											= 'Off Time Status online(SKYPE)';
$_['on_off_status_skype']      	 	 									= 'ON OFF STATUS Skype';
$_['entry_color_skype']      	 	 									= 'Change the color of icons Skype:';
$_['entry_title_schedule']      	 	 								= 'Title Block Schedule:';
$_['entry_color_clock']      	 	 									= 'Change the color of icons schedule:';
$_['entry_daily']      	 	 											= 'daily:';
$_['entry_weekend']      	 	 										= 'Output:';
$_['entry_background_callback']      	 	 							= 'Choose a background callback:';
$_['entry_background_button_callback']      	 	 					= 'Change the color of the button to order a return call:';
$_['entry_background_button_callback_hover']      	 	 				= 'Change the color of the button when the mouse request a call back:';
$_['entry_phone_number_send_sms']      	 	 							= 'Enter the number for receiving SMS:';
$_['entry_login_send_sms']      	 	 								= 'Username that was registered on the site:';
$_['entry_pass_send_sms']      	 	 									= 'Password that is recorded on the website:';
$_['button_callback_setting']      	 	 								= 'Settings';
$_['heading_title_callback']      	 	 								= 'CALLBACK';
$_['register_site']      	 	 										= 'To register on the site =>';
$_['entry_social_callback']      	 	 								= 'Add social network';
$_['add']      	 	 													= 'Add';
$_['button_remove']      	 	 										= 'Delete';
$_['text_time_callback']      	 	 									= 'How many call';
$_['text_topic_callback']      	 	 									= 'Subject call';


/*******/

$_['text_setting_module'] 												= 'Settings module Callback';
$_['tab_fields_setting'] 												= 'Form Field';
$_['tab_general_setting'] 												= 'General settings';
$_['tab_contact_setting'] 												= 'Contact';
$_['tab_design_setting'] 												= '<span style="color:red">[TURNING MODULE]</span>';
$_['tab_sms_setting'] 													= 'SMS Settings';
$_['tab_email_setting'] 												= 'Customize Template Letters';

$_['entry_mask_phone_number'] 											= 'Edit Mask phone +3(999) 999-99-99';

$_['text_status_fields'] 												= 'Status field';
$_['text_requared_fields'] 												= 'Required';
$_['text_placeholder_fields'] 											= 'Placeholder';
$_['text_on_off_fields_firstname'] 										= 'Field - Name -';
$_['text_on_off_fields_phone'] 											= 'Field - Telephone -';
$_['text_on_off_fields_comment'] 										= 'Field - Comment -';
$_['text_on_off_fields_email'] 											= 'Field - Email -';
$_['text_complete_quickorder'] 											= 'The text following the successful registration of the order!';
$_['on_off_sms_callback'] 												= 'Sending sms to you [ADMIN]!';
$_['text_delete'] 														= 'Delete';
$_['text_add'] 															= 'Add';
$_['entry_social_block_title'] 											= 'Title block social networks';
$_['entry_any_text_bottom_before_button'] 								= 'Add any text before button to request a call back';
$_['form_latter_from_me_callback'] 										= 'A template letter for you [Admin Store]!';
$_['text_on_off_send_me_mail_callback'] 								= 'Send an e-mail to you [Admin]';
$_['callback_subject'] 													= 'Letter subject';
$_['subject_text_variables'] 											= 'It supports variable';
$_['entry_you_email_callback'] 											= 'Email which will come to the notice';
$_['quickorder_description_me'] 										= 'Email Template';
$_['text_select_design_theme_callback'] 								= 'Select Design Popup window';
$_['entry_background_callback_hover'] 									= 'Choose a background call back when you hover';
$_['text_on_off_contact_right'] 										= 'Contact on / off';
$_['error_permission'] 													= 'Warning: You do not have permission to modify bestsellers module!';
$_['text_theme2_left'] 													= 'View left';
$_['text_theme2_right'] 												= 'View right';
$_['text_theme_callback'] 												= 'SUBJECT CALL';
$_['add_activation_key'] 												= 'Enter the license key:';
$_['heading_title_activation'] 											= 'Activation Module';
$_['activated_btn'] 													= 'Activate!';
$_['the_module_is_activated'] 											= 'The module is activated!';
$_['key_success_deactivation'] 											= 'Module deactivation!';
$_['this_key_is_not_present_enter_the_correct_key'] 					= '<span style="color:red;">Error:</span> this key is not present, enter the correct key';
$_['key_error_deactivation'] 											= '<span style="color:red;">Error:</span> Unable to deactivation key';
$_['enter_key_deactivation'] 											= '<span style="color:red;">Error:</span> deactivation key is not specified.!';
$_['enter_deactivation_key'] 											= 'Enter key Deactivation';
$_['btn_deactivation'] 													= 'Deactivation key';

$_['list_of_variables_entry'] 						= '
<table><tr><td>
<br/><b>~name~</b><i style="font-weight:400"> - Customer Name</i>
<br/><b>~phone~</b><i style="font-weight:400"> - Telephone</i>
<br/><b>~comment_buyer~</b><i style="font-weight:400"> - Comment</i>
<br/><b>~email_buyer~</b><i style="font-weight:400"> - email</i>      
<br/><b>~store_name~</b><i style="font-weight:400"> - Store name</i>             
<br/><b>~time_callback_on~</b><i style="font-weight:400"> - call from </i>             
<br/><b>~time_callback_off~</b><i style="font-weight:400"> - call for</i>             
<br/><b>~topic_callback_send~</b><i style="font-weight:400"> - Subject Bell</i>             
</td></tr></table><br/>
';



?>
