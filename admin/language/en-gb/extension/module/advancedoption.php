<?php
// Heading
$_['heading_title']    = '<a>Advanced Option Pro Plus</a> <br/> &nbsp; By Extensions Bazaar <a href="http://prowebber.ru" target="_blank" title="prowebber.ru" style="color:#0362B6;margin-right:5px"><i class="fa fa-cloud-download fa-fw"></i></a> ';
$_['heading_inner_title']    = 'Advanced Option Pro Plus - V1.0';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Advanced Options!';
$_['text_edit']        = 'Edit Advanced Options';
$_['text_color']       = 'Color';
$_['text_image']       = 'Image';
$_['text_squre']       = 'Square';
$_['text_round']       = 'Round';

// Entry
$_['entry_status']     = 'Status';
$_['tab_front']        = 'Front Store';
$_['tab_language']     = 'Language';


// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Advanced Options!';