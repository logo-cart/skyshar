<?php
// Heading
$_['heading_title']    = 'Random Products from specific category';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Random products module!';
$_['text_edit']        = 'Edit Random products Module';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_limit']      = 'Number of random products';
$_['entry_category']   = 'Choose category';
$_['entry_status']     = 'Status';
$_['entry_width']      = 'Product image width';
$_['entry_height']     = 'Product image height';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Random products!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';
$_['error_width']      = 'Width required!';
$_['error_height']     = 'Height required!';