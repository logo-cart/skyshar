<?php
// HTTP
define('HTTP_SERVER', 'https://skyshar.com.ua/admin/');
define('HTTP_CATALOG', 'https://skyshar.com.ua/');

// HTTPS
define('HTTPS_SERVER', 'https://skyshar.com.ua/admin/');
define('HTTPS_CATALOG', 'https://skyshar.com.ua/');

// DIR
define('DIR_APPLICATION', '/home/aerodesi/skyshar.com.ua/www/admin/');
define('DIR_SYSTEM', '/home/aerodesi/skyshar.com.ua/www/system/');
define('DIR_IMAGE', '/home/aerodesi/skyshar.com.ua/www/image/');
define('DIR_STORAGE', '/home/aerodesi/skyshar.com.ua/storage/');
define('DIR_CATALOG', '/home/aerodesi/skyshar.com.ua/www/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'aerodesi.mysql.tools');
define('DB_USERNAME', 'aerodesi_aeronew');
define('DB_PASSWORD', '9dlZ%D@m52');
define('DB_DATABASE', 'aerodesi_aeronew');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
