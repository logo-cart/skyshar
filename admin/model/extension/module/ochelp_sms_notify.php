<?php
class ModelExtensionModuleOchelpSmsNotify extends Model {
	public function getPaymentList() {
		$query = $this->db->query("SELECT code FROM  `" . DB_PREFIX . "extension` WHERE type = 'payment'");

		$payments = array();

		if ($query->num_rows) {
			foreach ($query->rows as $payment) {
				$this->load->language('extension/payment/' . $payment['code'], 'extension');

				$payments[] = array(
					'name' => $this->language->get('extension')->get('heading_title'),
					'code' => $payment['code'],
				);
			}
		}

		return $payments;
	}

	public function getOrdersPhone($data = array()) {
		$result = array();

		if (isset($data['selected'])) {
			foreach ($data['selected'] as $order_id) {
				$sql = "SELECT telephone FROM `" . DB_PREFIX . "order` WHERE order_id = '" . (int) $order_id . "'";

				if (isset($data['sms_order_status']) && $data['sms_order_status']) {
					$sql .= " AND order_status_id = '" . (int) $data['sms_order_status'] . "'";
				}

				$query = $this->db->query($sql);

				if ($query->num_rows && $query->row['telephone']) {
					$result[] = preg_replace('/[^0-9,]/', '', $query->row['telephone']);
				}

			}
		} else if (isset($data['sms_order_status']) && $data['sms_order_status']) {

			$sql = "SELECT DISTINCT telephone FROM `" . DB_PREFIX . "order` WHERE order_status_id = '" . (int) $data['sms_order_status'] . "'";

			$query = $this->db->query($sql);

			if ($query->num_rows) {
				foreach ($query->rows as $row) {
					$result[] = preg_replace('/[^0-9,]/', '', $row['telephone']);
				}
			}
		}

		return $result;
	}

	public function addOrderHistory($order_id, $order_status_id, $message) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$order_status_id . "', notify = '1', comment = '" . $this->db->escape($message) . "', date_added = NOW()");		
	}
}