<?php
class ControllerExtensionModuleOcmodeditor extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/ocmodeditor');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->addScript('view/javascript/ocmodeditor/codemirror.js');
		$this->document->addStyle('view/stylesheet/ocmodeditor/codemirror.css');
		$this->document->addStyle('view/stylesheet/ocmodeditor/ocmodeditor.css');
		$this->load->model('setting/modification');
		if (isset($this->request->get['modification_id'])) {
			$mod_id = $this->request->get['modification_id'];
		} else {
			$mod_id = 0;
		}
		
		$data['modification_id'] = $mod_id;
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => 'Modifications',
			'href' => $this->url->link('marketplace/modification', 'user_token=' . $this->session->data['user_token'], true)
		);
		
		$data['save'] = html_entity_decode($this->url->link('extension/module/ocmodeditor/save', 'user_token=' . $this->session->data['user_token'], true));
		$data['cancel'] = $this->url->link('marketplace/modification', 'user_token=' . $this->session->data['user_token'], true);
		$data['download'] = $this->url->link('extension/module/ocmodeditor/download', 'user_token=' . $this->session->data['user_token'].'&modification_id='.$mod_id, true);
        $data['locate_affected_url'] = html_entity_decode($this->url->link('extension/module/ocmodeditor/locateAffected', 'user_token=' . $this->session->data['user_token'], true));

		$data['heading_title'] = $this->language->get('heading_title');

		$data['column_name'] = $this->language->get('column_name');
		$data['column_author'] = $this->language->get('column_author');
		$data['column_version'] = $this->language->get('column_version');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_download'] = $this->language->get('button_download');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_link'] = $this->language->get('button_link');
		$data['button_enable'] = $this->language->get('button_enable');
		$data['button_disable'] = $this->language->get('button_disable');
		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_locate_affected'] = $this->language->get('button_locate_affected');

        $data['text_affected_files'] = $this->language->get('text_affected_files');

		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		if ($mod_id) {
			$modification = $this->model_setting_modification->getModification($mod_id);
		} else if (!empty($this->session->data['ocmodeditor_add'])) {
			$modification = $this->session->data['ocmodeditor_add'];
		} else {
			$modification = false;
		}
		
		$data['modification_name'] = $modification['name'];
		$data['modification_xml'] = json_encode($modification['xml']);
		$data['breadcrumbs'][] = array(
			'text' => $data['modification_name'],
			'href' => $this->url->link('extension/module/ocmodeditor', 'user_token=' . $this->session->data['user_token'] . '&modification_id='.$mod_id, true)
		);

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/ocmodeditor', $data));
	}
	
	public function download() {
		if (isset($this->request->get['modification_id'])) {
			$mod_id = $this->request->get['modification_id'];
			$this->load->model('setting/modification');
			$modification = $this->model_setting_modification->getModification($mod_id);
			header('Content-Type: text/xml');
			header("Content-Transfer-Encoding: Binary"); 
			header("Content-disposition: attachment; filename=\"" . strtolower(preg_replace('/\s+/', '_', $modification['name'])) . ".xml\"");
			echo $modification['xml'];
			exit;
		}
	}
	
	public function save() {
		if (isset($this->request->post['modification_id']) && isset($this->request->post['xml'])) {
			$this->request->post['xml'] = html_entity_decode($this->request->post['xml']);
			$xml = new DOMDocument('1.0', 'UTF-8');
			$xml->preserveWhiteSpace = false;
			set_error_handler(array($this, 'exception_error_handler'));
			
			try {
				$xml->loadXml($this->request->post['xml']);
				$name = $xml->getElementsByTagName('name')->item(0)->textContent;
				$version = $xml->getElementsByTagName('version')->item(0)->textContent;
				$link = $xml->getElementsByTagName('link')->item(0)->textContent;
				$author = $xml->getElementsByTagName('author')->item(0)->textContent;
				$code = $xml->getElementsByTagName('code')->item(0)->textContent;
			} catch (Exception $e) {
				header('Content-Type: application/json');
				echo json_encode(array(
					'error' => true,
					'msg' => $e->getMessage()
				));
				exit;
			}
			
			if ((int)$this->request->post['modification_id']) {
				$updates = array();
				if (!empty($name)) {
					$updates[] = "name='" . $this->db->escape($name) . "'";
				}
				if (!empty($version)) {
					$updates[] = "version='" . $this->db->escape($version) . "'";
				}
				if (!empty($link)) {
					$updates[] = "link='" . $this->db->escape($link) . "'";
				}
				if (!empty($author)) {
					$updates[] = "author='" . $this->db->escape($author) . "'";
				}
				if (!empty($code)) {
					$updates[] = "code='" . $this->db->escape($code) . "'";
				}
				
				$updates[] = "xml='" . $this->db->escape($this->request->post['xml']) . "'";
				
				$this->db->query("UPDATE " . DB_PREFIX . "modification SET ".implode(', ', $updates)." WHERE modification_id=" . $this->request->post['modification_id']);
				
			} else if (!empty($this->session->data['ocmodeditor_add'])) {
				$this->load->model('setting/modification');
				$this->session->data['ocmodeditor_add']['name'] = !empty($name) ? $name : $this->session->data['ocmodeditor_add']['name'];
				$this->session->data['ocmodeditor_add']['version'] = !empty($version) ? $version : $this->session->data['ocmodeditor_add']['version'];
				$this->session->data['ocmodeditor_add']['link'] = !empty($link) ? $link : $this->session->data['ocmodeditor_add']['link'];
				$this->session->data['ocmodeditor_add']['author'] = !empty($author) ? $author : $this->session->data['ocmodeditor_add']['author'];
				$this->session->data['ocmodeditor_add']['code'] = !empty($code) ? $code : $this->session->data['ocmodeditor_add']['code'];
				$this->session->data['ocmodeditor_add']['xml'] = $this->request->post['xml'];
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "modification ORDER BY `extension_install_id` DESC");
				$extension_installer_id = $query->row['extension_install_id'];
				$this->session->data['ocmodeditor_add']['extension_install_id'] = (int)$extension_installer_id + 1;
				$this->model_setting_modification->addModification($this->session->data['ocmodeditor_add']);
				$this->session->data['ocmodeditor_new_mod_id'] = $this->db->getLastId();
				unset($this->session->data['ocmodeditor_add']);
			}
			$this->session->data['ocmodeditor_save'] = true;
			restore_error_handler();
			$this->load->controller('marketplace/modification/refresh', 
				array(
				'ISL_no_redirect'=>true,
				'redirect'=>$this->url->link('extension/module/ocmodeditor', 'user_token=' . $this->session->data['user_token'] . '&modification_id='. $this->request->post['modification_id'], true)));
			$this->load->language('extension/module/ocmodeditor');
			
			
				header('Content-Type: application/json');
				echo json_encode(array(
					'error' => false,
					'msg' => $this->language->get('text_success')
				));
				exit;
		}
	}

    public function locateAffected() {
		if (isset($this->request->get['modification_id'])) {
			set_error_handler(array($this, 'exception_error_handler'));
			
			try {
                $GLOBALS['ocmodeditor_locate'] = $this->request->get['modification_id'];
                $this->load->controller('marketplace/modification/refresh');
                restore_error_handler();
			} catch (Exception $e) {
                throw $e;
                echo $e->getMessage();
				exit;
			}
        }
    }
	
	public function add() {
		$this->load->model('setting/modification');
		$xml = '<modification>
	<name></name>
	<version></version>
	<link></link>
	<author></author>
	<code></code>
	<file path="">
		<operation>
			<search><![CDATA[ ]]></search>
			<add position="before"><![CDATA[ ]]></add>
		</operation>
	</file>
</modification>';
		$mod_data = array(
			'modification_id' => 0,
			'name' => 'New OCMOD',
			'author' => $this->user->getUserName(),
			'version' => '1.0',
			'link' => '',
			'code' => '',
			'status' => 1,
			'xml' => $xml
		);
		
		$this->session->data['ocmodeditor_add'] = $mod_data;
		$this->response->redirect($this->url->link('extension/module/ocmodeditor', 'user_token='.$this->session->data['user_token'], true));
	}
	
	public function exception_error_handler($errno, $errstr, $errfile, $errline ) {

		throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'marketplace/modification')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}
