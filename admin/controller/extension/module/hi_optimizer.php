<?php
if (!class_exists('ControllerExtensionModuleHiOptimizer')) {
  class ControllerExtensionModuleHiOptimizer extends Controller
  {
    private $error = array();

    public function index()
    {
      $this->control();
      $this->controller_module_hi_optimizer->index($this->registry);
    }

    public function uninstall()
    {
      if ($this->validate()) {
        $this->control();
        if(method_exists('ControllerModuleHiOptimizer', 'uninstall'))
          $this->controller_module_hi_optimizer->uninstall($this->registry);
      }
    }

    public function install()
    {
      if ($this->validate()) {
        $this->control();
        if(method_exists('ControllerModuleHiOptimizer', 'install'))
          $this->controller_module_hi_optimizer->install($this->registry);
      }
    }

    protected function validate()
    {
      if (!$this->user->hasPermission('modify', 'extension/module/hi_optimizer')) {
        $this->error['warning'] = $this->language->get('error_permission');
      }
      return !$this->error;
    }

    public function control()
    {
      $file = DIR_APPLICATION . 'controller/module/hi_optimizer.php';
      $class = 'ControllerModuleHiOptimizer';
      if (file_exists($file)) {
        require_once $file;
        $this->registry->set('controller_module_hi_optimizer', new $class($this->registry));
      } else {
        trigger_error('Error: Could not load controller ' . 'module/hi_optimizer' . '!');
        exit();
      }
    }
  }
}
?>