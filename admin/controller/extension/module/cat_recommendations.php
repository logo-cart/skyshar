<?php
	class ControllerExtensionModuleCatRecommendations extends Controller {
		private $error = array();
		
		public function index() {
			$this->load->language('extension/module/cat_recommendations');
			
			$this->document->setTitle($this->language->get('heading_title'));
			
			$this->load->model('setting/module');
			
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
				if (!isset($this->request->get['module_id'])) {
					$this->model_setting_module->addModule('cat_recommendations', $this->request->post);
					} else {
					$this->model_setting_module->editModule($this->request->get['module_id'], $this->request->post);
				}
				
				$this->session->data['success'] = $this->language->get('text_success');
				
				$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
			}
			
			if (isset($this->error['warning'])) {
				$data['error_warning'] = $this->error['warning'];
				} else {
				$data['error_warning'] = '';
			}
			
			if (isset($this->error['name'])) {
				$data['error_name'] = $this->error['name'];
				} else {
				$data['error_name'] = '';
			}
			
			if (isset($this->error['width'])) {
				$data['error_width'] = $this->error['width'];
				} else {
				$data['error_width'] = '';
			}
			
			if (isset($this->error['height'])) {
				$data['error_height'] = $this->error['height'];
				} else {
				$data['error_height'] = '';
			}
			
			$data['breadcrumbs'] = array();
			
			$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
			);
			
			$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
			);
			
			if (!isset($this->request->get['module_id'])) {
				$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/cat_recommendations', 'user_token=' . $this->session->data['user_token'], true)
				);
				} else {
				$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/module/cat_recommendations', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'], true)
				);
			}
			
			if (!isset($this->request->get['module_id'])) {
				$data['action'] = $this->url->link('extension/module/cat_recommendations', 'user_token=' . $this->session->data['user_token'], true);
				} else {
				$data['action'] = $this->url->link('extension/module/cat_recommendations', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'], true);
			}
			
			$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
			
			if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
				$module_info = $this->model_setting_module->getModule($this->request->get['module_id']);
			}
			
			$data['user_token'] = $this->session->data['user_token'];
			
			if (isset($this->request->post['name'])) {
				$data['name'] = $this->request->post['name'];
				} elseif (!empty($module_info)) {
				$data['name'] = $module_info['name'];
				} else {
				$data['name'] = '';
			}
			
			//category
			$data['category_list'] = array();
			
			$this->load->model('catalog/category');
			$this->load->model('catalog/product');
			
			$categories = $this->model_catalog_category->getCategories(0);
			$data['categories'] = $categories;
			
			if (isset($module_info)) {
				foreach ($categories as $category) {
					$products = explode(',', $module_info['cat_recommendations_product_' . $category['category_id']]);
					
					$products_list = array();
					
					foreach ($products as $product_id) {
						$product_info = $this->model_catalog_product->getProduct($product_id);
						
						if ($product_info) {
							$products_list[] = array(
							'product_id' => $product_info['product_id'],
							'name'       => $product_info['name']
							);
						}
					}	
					
					$data['category_list'][] = array(
					'cat_recommendations_product' => $module_info['cat_recommendations_product_' . $category['category_id']],
					'category_id' => $category['category_id'],
					'name'       => $category['name'],
					'products'   => $products_list
					);
					
				}
				
				}else{
			    $products_list = array();
				
				foreach ($categories as $category) {
					$data['category_list'][] = array(
					'cat_recommendations_product' => '',
					'category_id' => $category['category_id'],
					'name'       => $category['name'],
					'products'   => $products_list
					);
				}
				
			}
			
			if (isset($this->request->post['width'])) {
				$data['width'] = $this->request->post['width'];
				} elseif (!empty($module_info)) {
				$data['width'] = $module_info['width'];
				} else {
				$data['width'] = 200;
			}
			
			if (isset($this->request->post['height'])) {
				$data['height'] = $this->request->post['height'];
				} elseif (!empty($module_info)) {
				$data['height'] = $module_info['height'];
				} else {
				$data['height'] = 200;
			}
			
			if (isset($this->request->post['status'])) {
				$data['status'] = $this->request->post['status'];
				} elseif (!empty($module_info)) {
				$data['status'] = $module_info['status'];
				} else {
				$data['status'] = '';
			}
			
			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');
			
			$this->response->setOutput($this->load->view('extension/module/cat_recommendations', $data));
		}
		
		public function autocomplete() {
			$json = array();
			
			if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_category_id'])) {
				$this->load->model('catalog/product');
				$this->load->model('catalog/option');
				
				if (isset($this->request->get['filter_name'])) {
					$filter_name = $this->request->get['filter_name'];
					} else {
					$filter_name = '';
				}
				
				if (isset($this->request->get['filter_category_id'])) {
					$filter_category_id = $this->request->get['filter_category_id'];
					} else {
					$filter_category_id = '';
				}
				
				
				if (isset($this->request->get['limit'])) {
					$limit = $this->request->get['limit'];	
					} else {
					$limit = 20;	
				}			
				
				$data = array(
				'filter_name'  => $filter_name,
				'filter_category_id' => $filter_category_id,
				'start'        => 0,
				'limit'        => $limit
				);
				
				$results = $this->model_catalog_product->catRecomGetProducts($data);
				
				foreach ($results as $result) {
					$json[] = array(
					'product_id' => $result['product_id'],
					'name'       => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),	
					);	
				}
			}
			
			$this->response->setOutput(json_encode($json));
		}
		
		protected function validate() {
			if (!$this->user->hasPermission('modify', 'extension/module/cat_recommendations')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
			
			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
				$this->error['name'] = $this->language->get('error_name');
			}
			
			if (!$this->request->post['width']) {
				$this->error['width'] = $this->language->get('error_width');
			}
			
			if (!$this->request->post['height']) {
				$this->error['height'] = $this->language->get('error_height');
			}
			
			return !$this->error;
		}
	}
