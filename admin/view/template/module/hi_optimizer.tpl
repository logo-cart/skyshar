<?php
if(empty($data) && !empty($this->data_for_tpl)) {
  $data = &$this->data_for_tpl;
}
if(empty($data)) $data = [];
$GLOBALS['data_globals'] = &$data;
$question_mark = $qm = ' <i class="fas fa-question-circle"></i>';

$checkbox_parameters_css = ['css_optimize', 'css_third_party_files_async', 'font_display_swap_enabled', 'css_merge_files', 'css_remove_duplicates', 'css_minify'];
$checkbox_parameters_css2 = ['link_parameter_enable_for_comparison'];

$checkbox_parameters_custom_scripts_link_optimize_disable = ['custom_scripts_link_optimize_disable'];
$checkbox_parameters_custom_scripts_optimize_disable = ['custom_scripts_optimize_disable'];

$checkbox_parameters1 = ['script_optimize', 'all_js_preload', 'lightbox_optimize', 'owl_carousel_optimize',
  'owl_carousel_preload', 'jquery_first_of_all', 'js_remove_duplicates', 'js_duplicates_without_parameter'];
$checkbox_parameters2 = ['iframe_lazy_off'];

$checkbox_js_parametrs = [];

$checkbox_yandex_parametrs = ['ya_metrika_opti_off', 'yandex_constructor_opti_off', 'yandex_chat_opti_off', 'yandex_kassa_opti_off'];
$checkbox_google_parametrs = ['google_analytics_off', 'google_tag_manager_opti_off'];

$textarea_parameters = ['route_exclusion', 'uri_start_exclusion', 'uri_strict_exclusion'];


$textarea_parameters_exclusion_of_css = ['css_exclusion_for_merger'];
$textarea_js_link_for_deferred_scripts = ['js_link_for_deferred_scripts'];

if(!function_exists('echo_')) {

  function echo_($var_name, $data= null) {
    if(empty($data)) $data = $GLOBALS['data_globals'];
    if(!empty($var_name))
    {
      if(!is_array($var_name)) {
        if(!empty($data[$var_name])) echo $data[$var_name];
      }
      else {
        if(!empty($data[$var_name[0]][$var_name[1]])) echo $data[$var_name[0]][$var_name[1]];
      }
    }
  }
}

function checkbox_parameters($checkbox_parameters, $data = null, $question_mark = ' <i class="fas fa-question-circle"></i>')
{
  if(empty($data)) $data = $GLOBALS['data_globals'];
  $qm = $question_mark;
  foreach ($checkbox_parameters as $parameter) {
    $t = 'tooltip_' . $parameter;
    $v = 'text_' . $parameter; ?>
    <div class="form-check" id="form_check_<?php echo $parameter; ?>">
      <input class="form-check-input" type="checkbox" id="<?php echo $parameter; ?>"
             name="<?php echo $parameter; ?>" <?php if (!empty($data[$parameter]) && strtolower($data[$parameter]) != 'off') echo 'checked'; ?>>
      <label class="form-check-label" for="<?php echo $parameter; ?>" data-toggle="tooltip"
             title="<?php echo_($t, $data); ?>">
        <?php echo_($v, $data);
        if (!empty($data[$t])) echo $qm; ?>
      </label>
    </div>
  <?php }
}


function input_field($parameter, $placeholder = null, $default_value = null, $data = null, $question_mark = ' <i class="fas fa-question-circle"></i>'){
  if(empty($data)) $data = $GLOBALS['data_globals'];
  $qm = $question_mark;
  $t = 'tooltip_'. $parameter; $v = 'text_'. $parameter; ?>
  <label style="margin-top: 10px; margin-bottom: 10px;" class="form-check-label" for="jquery_file_name" data-toggle="tooltip" title="<?php echo_($t, $data); ?>">
    <?php echo_($v, $data); if(!empty($data[$t])) echo $qm; ?>
  </label>
  <input type="text" id="<?php echo $parameter; ?>" name="<?php echo $parameter; ?>" class="form-control"
         placeholder="<?php if(!empty($placeholder)) echo $placeholder; ?>" value="<?php
  if(!empty($data[$parameter])) echo $data[$parameter];
  elseif(!empty($default_value)) echo $default_value;
  else echo '';
  ?>">

<?php }

// может быть как строкой, так и массивом $parameters
// т.е. либо одно поле textarea, либо несколько
function textarea_field($parameters, $placeholders = null, $default_values = null, $data = null, $question_mark = ' <i class="fas fa-question-circle"></i>'){
  if(empty($data)) $data = $GLOBALS['data_globals'];
  $qm = $question_mark;

  if(!is_array($parameters)) {
    $parameter = $parameters;
    $parameters = [];
    $parameters[] = $parameter;
  }

  if(!is_array($placeholders)) {
    $placeholder = $placeholders;
    $placeholders = [];
    $placeholders[] = $placeholder;
  }

  if(!is_array($default_values)) {
    $default_value = $default_values;
    $default_values = [];
    $default_values[] = $default_value;
  }

  foreach ($parameters as $parameter) {
    $t = 'tooltip_'. $parameter; $v = 'text_'. $parameter; ?>
    <label style="margin-top: 10px; margin-bottom: 10px;" class="form-check-label" for="<?php echo $parameter; ?>" data-toggle="tooltip" title="<?php echo_($t, $data); ?>">
      <?php echo_($v, $data); if(!empty($data[$t])) echo $qm; ?>
    </label>
    <textarea id="<?php echo $parameter; ?>" name="<?php echo $parameter; ?>" class="form-control"
              placeholder="<?php if(!empty($placeholder)) echo $placeholder; ?>"><?php ?><?php
      if(!empty($data[$parameter])) echo $data[$parameter];
      elseif(!empty($default_value)) echo $default_value;
      else echo '';
      ?></textarea>

  <?php }
  }



$nav_tabs = ['main-panel', 'home_page_analysis-panel', 'js_link-panel', 'js-panel', 'yandex-panel', 'google-panel', 'jivosite-panel', 'vk-panel', 'facebook-panel', 'page_exclusion-panel'];

function nav_tabs($nav_tabs, $data = null) {
  if(empty($data)) $data = $GLOBALS['data_globals']; ?>
<ul class="nav  nav-pills" style=" width: 100%; justify-content: flex-start;">
  <?php
  foreach ($nav_tabs as $i => $nav_tab) { ?>

    <li class="nav-item btn-sm">
        <a class="nav-link <?php if($i==0) echo 'active'; ?>"
           id="<?php echo "$nav_tab-tab";  ?>" data-toggle="tab" href="#<?php echo "$nav_tab";  ?>"
           role="tab" aria-controls="<?php echo "$nav_tab";  ?>" aria-selected="<?php if($i==0) echo 'true'; else echo 'false';  ?>>">
          <?php echo_("text_$nav_tab-tab");  ?>
        </a>
      </li>

  <?php  }
  ?>
</ul>

  <?php
}


function nav_pills_vertical($nav_tabs, $data = null) {
  if(empty($data)) $data = $GLOBALS['data_globals']; ?>
  <ul class="nav flex-column  nav-pills" style="">
    <?php
    foreach ($nav_tabs as $i => $nav_tab) { ?>

      <li class="nav-item btn-sm">
        <a class="nav-link mytooltip_ <?php if($i==0) echo 'active'; ?>"
           id="<?php echo "$nav_tab-tab";  ?>" data-toggle="tab" href="#<?php echo "$nav_tab";  ?>"  title="<?php echo_("tooltip_$nav_tab-tab"); ?>"
           role="tab" aria-controls="<?php echo "$nav_tab";  ?>" aria-selected="<?php if($i==0) echo 'true'; else echo 'false';  ?>>">
          <?php echo_("text_$nav_tab-tab");  ?>
          <?php if(!empty($data["tooltip_$nav_tab-tab"])) { ?>
          <i class="fas fa-question-circle"></i>
          <?php } ?>
        </a>
      </li>

    <?php  }
    ?>
  </ul>

  <?php
}
?>



<!DOCTYPE html>
<html dir="ltr" lang="ru">
<head>
  <meta charset="UTF-8" />
  <title>Hi-Optimizer</title>
  <base href="<?php if(!empty($base))echo $base; ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

  <link href="view/javascript/sitecreator/bootstrap-4.4.1/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
  <link href="view/javascript/sitecreator/fontawesome-5/css/all.min.css" type="text/css" rel="stylesheet" />
  <script type="text/javascript" src="view/javascript/sitecreator/jquery-3.4.1.min.js"></script>
  <script type="text/javascript" src="view/javascript/sitecreator/popper.js-1.16.0/umd/popper.min.js"></script>
  <script type="text/javascript" src="view/javascript/sitecreator/bootstrap-4.4.1/js/bootstrap.min.js"></script>

<!--  <link rel="stylesheet" href="view/javascript/sitecreator/highlight/styles/default.css">-->
  <link rel="stylesheet" href="view/javascript/sitecreator/highlight/styles/atom-one-light.css">
  <script src="view/javascript/sitecreator/highlight/highlight.pack.js"></script>
  <script src="view/javascript/sitecreator/highlight//highlightjs-line-numbers.js"></script>
  <script>
    hljs.initHighlightingOnLoad();
    hljs.initLineNumbersOnLoad();
  </script>




<style>
  html {
    font-size: 14px;
  }

  .container-fluid {
    padding-left: 30px;
    padding-right: 30px;
  }
  .nav_links {
    margin-top: 12px;
  }

  .nav_links a {
    padding: 3px 15px 3px 0;
    margin: 0px 25px 0 0;
    font-size: 12px;
  }
  .my_lable {
    /*font-size: 13px;*/
    margin-bottom: 10px;
  }
  .scr_copyright {
    /*background: #e7e7e7;*/
    font-size: 12px;
  }
  .scr_copyright a {
    padding: 3px 8px 3px 0;
    margin: 0 15px 0 0;
    font-size: 12px;
  }
  .scr_copyright h4 {
    margin-bottom: 0; color: #5b5b5b; font-size: 18px;
  }
  footer {
    margin: 30px 0;
    padding: 15px 0;
    border-top: #dcdcdc 1px solid;
  }
  .opencart_ver {
    display: inline-block;
    padding: 10px 0;
    color: #5b5b5b;
  }

  #btn_moluleONorOFF, #clear_module_cache {
    min-width: 150px;
    margin-right: 5px;
  }

  #form_check_css_third_party_files_async, #form_check_css_merge_files,
  #form_check_font_display_swap_enabled, #form_check_facebook_optimize_sdk_doubles_off {
    padding-left: 35px;
  }

  #form_check_facebook_pixel_off {
    padding-top: 15px;
  }

  #form_check_font_displajquery-3.4.1.miny_swap_enabled {
    margin-bottom: 8px;
  }
  #form_check_css_third_party_files_lowest_priority, #form_check_owl_carousel_preload {
    padding-left: 55px;
    margin-bottom: 8px;
  }



  #form_check_css_remove_duplicates, #form_check_css_minify {
    padding-left: 55px;
  }

  label[for="js_optimize_type"], label[for="css_cache_time"] {

    margin-top: 12px;
  }
  select#js_optimize_type, select#css_cache_time {
    margin: 10px 0 20px;
  }
  #form_select_css_cache_time {
    /*padding-left: 70px;*/
  }
  #jquery_first_of_all, #owl_carousel_optimize, #lightbox_optimize, #js_remove_duplicates, #all_js_preload {
    margin-left: 0px;
  }
  #jquery_first_of_all+label.form-check-label,
  #lightbox_optimize+label.form-check-label,
  #owl_carousel_optimize+label.form-check-label,
  #js_remove_duplicates+label.form-check-label,
  #all_js_preload+label.form-check-label{
    margin-left: 20px;
  }
  #owl_carousel_preload+label.form-check-label {
    margin-left: 5px;
  }

  #js_duplicates_without_parameter {
    margin-left: 20px;
  }

  #js_duplicates_without_parameter+label.form-check-label {
    margin-left: 40px;
  }


  #module_status {
    font-size: 12px;
    display: inline-block;
    margin-top: 12px;
    min-width: 100px;

  }
  #module_status span {

  }

  .sidebar {
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    z-index: 100;
    padding: 48px 0 0;
    box-shadow: inset -1px 0 0
    rgba(0, 0, 0, .1);
  }


  .sidebar-sticky {
    position: -webkit-sticky;
    position: sticky;
    top: 0;
    height: calc(100vh - 160px);

    overflow-x: hidden;
    overflow-y: auto;

  }

  #left_munu {
    padding: 25px 10px;
  }

  .module_copyright {
    margin-bottom: 10px;
  }

  footer {
    /*position: fixed;*/
    /*bottom: 0;*/
    background: #fff;
  }

  #nav_left_footer {
    position: fixed;
    bottom: 0;
    background: #f1f1f1;
    border-top: 1px solid #e5e5e5;
    padding-top: 10px;
    padding-bottom: 5px;
  }

  .sitecreator_logo {
    margin: 10px 0;

  }
  .sitecreator_logo img {
    max-width: 100%;
  }
  #left_sidebar {

  }

  select.form-control {
    max-width: 350px;
  }
  input.form-control, textarea.form-control {
    width: 50%;
    min-width: 200px;
    max-width: 1000px;
  }

  label.form-check-label {
    /*width: 50%;*/
    /*min-width: 200px;*/
    /*max-width: 1000px;*/
  }

  a.nav-link.mytooltip:not(.active):hover, a.nav-link.mytooltip_:not(.active):hover {
    background: #ececec;
  }

  div.tab-pane {
    padding-top: 30px;
  }
  #home_page_analysis_result {
    border: solid 1px #dcdcdc;
    padding: 20px 20px;
    margin: 30px 0;
    width: 100%;
  }

  #your_website, .your_website, .your_website_title, .your_website_link {
    border: solid 1px #dcdcdc;
    padding: 8px 20px;
    font-size: 12px;
    line-height: 14px;
    margin: 10px 0px 15px 0;
    display: inline-block;
    border-radius: 3px;
  }

  .your_website_title+.your_website_link, #your_website_title+#your_website {

    /*margin-top: -18px;*/
  }

  .analyzed_block_name {
    border: solid 1px #dcdcdc;
    padding: 10px 20px;
    margin: 20px 0;
    font-weight: bold;
    color: #717171;
  }

  .code_start_position {
    font-weight: normal;
  }

  .panel-info {
    margin: 30px 0;
    border-top: 1px solid #dcdcdc;
    padding: 15px 0;

  }

  .panel-info {
    column-count: 2;
    /*column-width: 400px;*/
  }

  .panel-info img {
    max-width: 100%;
  }

  #vk_the_first_script {
    border: solid 1px #dcdcdc;
    padding: 20px 20px;
    margin: 30px 0;
    width: 100%;
    min-height: 200px;
  }

  textarea.form-control.add_script {
    margin-top: 20px;
    margin-bottom: 20px;
  }


   /*li.L0, li.L1, li.L2, li.L3,*/
   /*li.L5, li.L6, li.L7, li.L8*/
   /*{ list-style-type: decimal !important }*/

  /*If your needs cool style, add styles by taste:*/

  pre code {
    font-size: 14px;
  }

    /* for block of numbers */
  .hljs-ln-numbers, td.hljs-ln-numbers {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;

    text-align: right;
    color: #ccc;
    border-right: 1px solid #CCC;
    vertical-align: top;
    padding-right: 8px;

    /* your custom style here */
  }

  /* for block of code */
  .hljs-ln-code, td.hljs-ln-code {
    padding-left: 10px;
  }

  /*.hljs-ln td {*/
  /*  padding: 10px;*/
  /*}*/

  .alert.alert-danger {
    width: 100%;
    margin-top: 10px;
    display: none;
  }

  .page_uri_for_analysys {
    margin: 0px 20px 10px 0;
  }
  #page_uri__for_page_analisys {
    /*margin-top: 10px;*/
  }

  textarea.form-control {
    margin: 0px 0 20px 0;
  }
  
  .page_analysis_universal_empty {
    background: #ffffca;
    padding: 10px 20px;
  }
</style>
      </head>
      <body>
<!--<body style="padding-left: 400px;">-->
      <nav id="main_nav" class="navbar   border-bottom  shadow-sm sticky-top bg-white flex-md-nowrap p-0"  style="padding-bottom: 10px;">
<!--        <nav id="main_nav" class="navbar   border-bottom  shadow-sm  bg-white flex-md-nowrap p-0"  style="padding-bottom: 10px;">-->
        <nav class="navbar bg-white" style="justify-content: space-between; align-items: center; align-content:stretch; width: 100%;">
          <div>
            <div style="display: inline-block;">
              <a id="module_name" style="" href="" onclick="window.location.assign(window.location.href); return false;"><div style="padding-right: 25px;"><h4 style="margin-bottom: 0; color: #5b5b5b; font-size: 18px;"><?php if(!empty($heading_title)) echo $heading_title;?></h4></div></a>
              <script>document.getElementById('module_name').href = window.location.href; </script>
            </div>

            <div class="nav_links" style="display: inline-block;">
              <a href="<?php echo_(['home', 'href'], $data); ?>"><?php echo_(['home', 'text'], $data); ?></a>
              <a href="<?php echo_(['modules', 'href'], $data); ?>"><?php echo_(['modules', 'text'], $data); ?></a>
              <a target="_blank" href="<?php echo_(['my_module_doc', 'href'], $data); ?>"><?php echo_(['my_module_doc', 'text'], $data); ?></a>
            </div>
          </div>
          <div style="color: red; font-size: 12px; margin-top: 12px;">
            <?php if(!empty($lic_date_expired)) {
              if(!empty($text_lic_date_expired)) echo $text_lic_date_expired;
              echo "$lic_date_expired";
            } ?>
          </div>

          <div>
            <div id="module_status" style="padding: .25rem .5rem;" data-toggle="tooltip" title="Состояние модуля: применен (или нет) модификатор OCMOD модуля">
              <?php if(!empty($text_module_status)) echo $text_module_status; ?>
            </div>
            <button id="btn_moluleONorOFF" class="btn btn-sm <?php if(!empty($btn_moluleONorOFF_class)) echo $btn_moluleONorOFF_class; ?>"><?php if(!empty($btn_moluleONorOFF_content)) echo $btn_moluleONorOFF_content; ?></button>
            <button id="clear_module_cache" class="btn btn-sm  btn-outline-secondary">Очистить кеш Hi-Optimizer</button>
            <div id="submit_btn_box" style="margin: 3px 0; display:inline-block;">
              <button id="btn_submit" class="btn btn-sm  btn-outline-primary" data-toggle="tooltip" title="Сохранить" type="submit" form="hi_optimizer_form"><i class="far fa-save"></i></button>
              <button id="btn_submit_alt" class="btn btn-sm  btn-outline-secondary" onclick="$('input#noclose').val(1);" data-toggle="tooltip" title="Сохранитьи остаться" type="submit" form="hi_optimizer_form"><i class="far fa-save"></i></button>
              <a href="<?php if(!empty($cancel)) echo $cancel; ?>" class="btn btn-sm  btn-outline-secondary" data-toggle="tooltip" title="Отменить" ><i class="fas fa-reply"></i></a>
            </div>




          </div>
          <div style="width: 100%">

          </div>
          <div style="text-align:right; width: 100%;">
                        <div title="Это title анализируемой страницы" class="your_website_title your_website">your website page title???</div>
            <div title="Это адрес (ссылка) анализируемой страницы" class="your_website_link">your website page link???</div>

          </div>
          <div style="text-align:right; width: 100%;">
            <label style="margin-right: 10px; display:inline-block;" class="form-check-label" data-toggle="tooltip" title="Выберите URI для анализа страницы. Список URI можно менять на вкладке `Анализ страниц сайта`.">URI <i class="fas fa-question-circle"></i></label>
            <select style="display: inline-block" id="page_uri_for_analysys_universal" class="page_uri_for_analysys form-control">
            </select>
            <button type="button" style="display: inline-block" onclick="return false;" id="home_page_analysis_universal_btn" class="btn btn-sm  btn-outline-secondary"><i class="fas fa-sync"></i> Analysis Start</button>


          </div>



          <div id="alert_danger" class="alert alert-danger"></div>
        </nav>

      </nav>

      <div id="main_container" class="container-fluid">

        <!--  border-bottom  shadow-sm fixed-top-->

        <div class="row" style="">
          <!--    col-sm-2 d-none d-md-block bg-light sidebar-->
          <nav id="left_sidebar" class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar" style="">
            <div class="sidebar-sticky">
              <div id="left_munu">
                <?php nav_pills_vertical($nav_tabs);  ?>
              </div>
              <div id="nav_left_footer" class="col-sm-3 col-md-2"><?php echo_('nav_left_footer', $data); ?></div>
            </div>

          </nav>
          <main class="col-sm-9 ml-sm-auto col-md-10 pt-3 px-4" role="main">

            <form action="<?php if(!empty($action)) echo $action; ?>" method="post" enctype="multipart/form-data" id="hi_optimizer_form" name="hi_optimizer_form">
              <div class="tab-content">
                <div class="tab-pane active" id="main-panel" role="tabpanel" aria-labelledby="main-panel-tab">
                  <div class="form-group">
                    <div class="">
                      <input value="0" hidden name="noclose" id="noclose">


                      <?php   foreach ($checkbox_parameters_css as $parameter) {$t = 'tooltip_'. $parameter; $v = 'text_'. $parameter;?>
                        <div class="form-check" id="form_check_<?php echo $parameter; ?>">
                          <input class="form-check-input" type="checkbox" id="<?php echo $parameter; ?>" name="<?php echo $parameter; ?>" <?php if(!empty($$parameter) && strtolower($$parameter) != 'off') echo 'checked'; ?>>
                          <label class="form-check-label" for="<?php echo $parameter; ?>" data-toggle="tooltip" title="<?php if(!empty($$t)) echo $$t; ?>">
                            <?php if(!empty($$v)) echo $$v; if(!empty($$t)) echo $qm; ?>
                          </label>
                        </div>

                      <?php } ?>


                      <?php
                      $parameter = "css_cache_time";
                      $t = 'tooltip_'. $parameter; $v = 'text_'. $parameter;

                      ?>



                      <div id="form_select_css_cache_time">
                        <label class="form-check-label" for="<?php echo $parameter; ?>" data-toggle="tooltip" title="<?php if(!empty($$t)) echo $$t; ?>">
                          <?php if(!empty($$v)) echo $$v; if(!empty($$t)) echo $qm; ?>
                        </label>
                        <select id="<?php echo $parameter; ?>" name="<?php echo $parameter; ?>" class="form-control">
                          <option <?php if(!empty($$parameter) && $$parameter == '3600') echo 'selected'; ?> value="3600">1 час</option>
                          <option <?php if(empty($$parameter) || $$parameter == '86400') echo 'selected'; ?> value="86400">1 сутки</option>
                          <option <?php if(!empty($$parameter) && $$parameter == '604800') echo 'selected'; ?> value="604800">7 дней</option>
                          <option <?php if(!empty($$parameter) && $$parameter == '2592000') echo 'selected'; ?> value="2592000">30 дней</option>

                        </select>
                      </div>

                      <?php
                      foreach ($textarea_parameters_exclusion_of_css as $parameter) {
                        $t = 'tooltip_'. $parameter;
                        $v = 'text_'. $parameter;
                        ?>
                        <label style="margin-bottom: 10px;" class="form-check-label my_lable" for="<?php echo $parameter; ?>" data-toggle="tooltip" title="<?php if(!empty($$t)) echo $$t; ?>"><?php if(!empty($$v)) echo $$v; if(!empty($$t)) echo $qm; ?></label>
                        <div class="input-group">
                          <textarea style="margin-bottom: 20px;"  class="form-control" id="<?php echo $parameter; ?>" name="<?php echo $parameter; ?>"><?php if(!empty($$parameter)) echo $$parameter; ?></textarea>
                        </div>
                      <?php } ?>


                      <?php   foreach ($checkbox_parameters_css2 as $parameter) {$t = 'tooltip_'. $parameter; $v = 'text_'. $parameter;?>
                        <div class="form-check" id="form_check_<?php echo $parameter; ?>">
                          <input class="form-check-input" type="checkbox" id="<?php echo $parameter; ?>" name="<?php echo $parameter; ?>" <?php if(!empty($$parameter) && strtolower($$parameter) != 'off') echo 'checked'; ?>>
                          <label class="form-check-label" for="<?php echo $parameter; ?>" data-toggle="tooltip" title="<?php if(!empty($$t)) echo $$t; ?>">
                            <?php if(!empty($$v)) echo $$v; if(!empty($$t)) echo $qm; ?>
                          </label>
                        </div>

                      <?php } ?>
                      <hr><br>

                      <?php   foreach ($checkbox_parameters1 as $parameter) {$t = 'tooltip_'. $parameter; $v = 'text_'. $parameter;?>
                        <div class="form-check" id="form_check_<?php echo $parameter; ?>">
                          <input class="form-check-input" type="checkbox" id="<?php echo $parameter; ?>" name="<?php echo $parameter; ?>" <?php if(!empty($$parameter) && strtolower($$parameter) != 'off') echo 'checked'; ?>>
                          <label class="form-check-label" for="<?php echo $parameter; ?>" data-toggle="tooltip" title="<?php if(!empty($$t)) echo $$t; ?>">
                            <?php if(!empty($$v)) echo $$v; if(!empty($$t)) echo $qm; ?>
                          </label>
                        </div>
                      <?php } ?>

                      <?php
                      $parameter = "js_optimize_type";
                      $t = 'tooltip_'. $parameter; $v = 'text_'. $parameter;

                      input_field('jquery_file_name');
                      ?>

                      <label class="form-check-label" for="<?php echo $parameter; ?>" data-toggle="tooltip" title="<?php if(!empty($$t)) echo $$t; ?>">
                        <?php if(!empty($$v)) echo $$v; if(!empty($$t)) echo $qm; ?>
                      </label>
                      <select id="<?php echo $parameter; ?>" name="<?php echo $parameter; ?>" class="form-control">
                        <option <?php if(empty($$parameter) || $$parameter == '0') echo 'selected'; ?> value="0">0) Отсутствует</option>
                        <option <?php if(!empty($$parameter) && $$parameter == '1') echo 'selected'; ?> value="1">1) Группирование в конце HTML</option>
                        <option <?php if(!empty($$parameter) && $$parameter == '2') echo 'selected'; ?> value="2">2) Продвинутый, вариант А</option>
                        <option <?php if(!empty($$parameter) && $$parameter == '3') echo 'selected'; ?> value="3">3) Продвинутый, вариант Б</option>
                      </select>

                      <hr><br>
                      <?php checkbox_parameters($checkbox_parameters2);  ?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="home_page_analysis-panel" role="tabpanel" aria-labelledby="home_page_analysis-panel-tab">
                  <br>
                  <script>
                    var page_uri__for_page_analisys = [];
                    page_uri__for_page_analisys.push('/');
                    <?php
                    if(!empty($data['page_uri__for_page_analisys'])) {
                      $uri_for_analisys = explode("\n", $data['page_uri__for_page_analisys']);
                      foreach ($uri_for_analisys as $uri) {
                        echo "page_uri__for_page_analisys.push(\"$uri\");\n";
                      }
                    }

                    ?>
                  </script>


                  <?php
                  textarea_field('page_uri__for_page_analisys', $data['placeholder_page_uri__for_page_analisys']);
?>

                  <textarea id="home_page_analysis_result">???</textarea>

                  <pre><code id="code_home_page_analysis_result" class="language-html prettyprint linenums html ">&lt;div class=&quot;test&quot;&gt;
test
&lt;/div&gt;</code></pre>

                </div>
                <div class="tab-pane" id="js_link-panel" role="tabpanel" aria-labelledby="js_link-panel-tab">

                  <?php   foreach ($checkbox_parameters_custom_scripts_link_optimize_disable as $parameter) {$t = 'tooltip_'. $parameter; $v = 'text_'. $parameter;?>
                    <div class="form-check" id="form_check_<?php echo $parameter; ?>">
                      <input class="form-check-input" type="checkbox" id="<?php echo $parameter; ?>" name="<?php echo $parameter; ?>" <?php if(!empty($$parameter) && strtolower($$parameter) != 'off') echo 'checked'; ?>>
                      <label class="form-check-label" for="<?php echo $parameter; ?>" data-toggle="tooltip" title="<?php if(!empty($$t)) echo $$t; ?>">
                        <?php if(!empty($$v)) echo $$v; if(!empty($$t)) echo $qm; ?>
                      </label>
                    </div>

                  <?php } ?>
                  <br>

                  <?php
                  textarea_field($textarea_js_link_for_deferred_scripts);
                   ?>
                  <div id="table_js_link_for_deferred_scripts"></div>
                  <div class="page_analysis_output"></div>
                </div>





                <div class="tab-pane" id="js-panel" role="tabpanel" aria-labelledby="js-panel-tab">
                  <?php   foreach ($checkbox_parameters_custom_scripts_optimize_disable as $parameter) {$t = 'tooltip_'. $parameter; $v = 'text_'. $parameter;?>
                    <div class="form-check" id="form_check_<?php echo $parameter; ?>">
                      <input class="form-check-input" type="checkbox" id="<?php echo $parameter; ?>" name="<?php echo $parameter; ?>" <?php if(!empty($$parameter) && strtolower($$parameter) != 'off') echo 'checked'; ?>>
                      <label class="form-check-label" for="<?php echo $parameter; ?>" data-toggle="tooltip" title="<?php if(!empty($$t)) echo $$t; ?>">
                        <?php if(!empty($$v)) echo $$v; if(!empty($$t)) echo $qm; ?>
                      </label>
                    </div>

                  <?php } ?>
                  <br>
                  <button type="button" id="btn_add_script" class="btn btn-sm  btn-outline-secondary" >Добавить скрипт</button><br><br>
                  <?php checkbox_parameters($checkbox_js_parametrs);  ?>

                  <?php

                  if(!empty($GLOBALS['data_globals']['script_added'])) {
                    if(empty($data)) $data = $GLOBALS['data_globals'];

                    $max_i = 0;
                    foreach ($data['script_added'] as $i => $val) {
                      $max_i = max($max_i, $i);
                      echo "<br><button type=\"button\"onclick=\"del_script_for_optimization($i, this);\" class=\"btn btn-sm  btn-outline-secondary\">удалить</button><textarea class=\"form-control add_script\" id='script_added_$i' name='script_added[$i]' >$val</textarea>";

                    }
                     echo "<script>window.script_added_count = $max_i; console.log('max index of array script_added', $max_i);</script>";

                  }

                  ?>
                  <div id="js-panel_end"></div>
                  <div class="panel-info"><?php echo_('text_deferred_javascript_info'); ?>
                  <?php
                  $file = DIR_APPLICATION. 'view/template/module/hioptimizer_info/deferred_javascript.tpl';
                  if(file_exists($file)) {require_once $file;}
                  ?>
                  </div>
                </div>
                <div class="tab-pane" id="yandex-panel" role="tabpanel" aria-labelledby="yandex-panel-tab">
                  <?php checkbox_parameters($checkbox_yandex_parametrs);  ?>

                  <br>
                  <button type="button" onclick="return false;" id="page_analysis_for_yandex_btn" class="btn btn-sm  btn-outline-secondary"><i class="fas fa-sync"></i> Page Analysis for Yandex Start</button>
                  <br><br>
                  <div class="your_website" id="your_website_of_page_analysis_for_yandex">???</div>
                  <div id="page_analysis_for_yandex_output"></div>
                </div>
                <div class="tab-pane" id="google-panel" role="tabpanel" aria-labelledby="google-panel-tab">
                  <?php checkbox_parameters($checkbox_google_parametrs);  ?>
                </div>
                <div class="tab-pane" id="jivosite-panel" role="tabpanel" aria-labelledby="jivosite-panel-tab">
                  <?php
                  checkbox_parameters(['jivo_site_off', 'jivo_site_is_nonstandard']);
                  input_field('jivo_site_id');
                  input_field('begin_jivo_site_code', '<!-- BEGIN JIVOSITE CODE {literal} -->', '<!-- BEGIN JIVOSITE CODE {literal} -->');
                  input_field('end_jivo_site_code', '<!-- {/literal} END JIVOSITE CODE -->', '<!-- {/literal} END JIVOSITE CODE -->');
                  ?>

                </div>
                <div class="tab-pane" id="vk-panel" role="tabpanel" aria-labelledby="vk-panel-tab">
                  <?php
                  checkbox_parameters(['vk_optimize_off', 'vk_link_of_api', 'vk_find_the_first_script']);
                  $parameter = "vk_optimize_mode";
                  $t = 'tooltip_'. $parameter; $v = 'text_'. $parameter;
                  ?>
                  <label></label>

                    <?php
                    textarea_field('vk_the_first_script', $data['text_vk_the_first_script_placeholder']);
                    ?>

                  <?php
                  checkbox_parameters(['vk_non_standard_init']);
                  input_field('vk_apiId', $data['text_vk_apiId_placeholder']);
                  ?>

                  <br>
                  <div id="form_select_vk_optimize_mode">
                    <label class="form-check-label" for="<?php echo $parameter; ?>" data-toggle="tooltip" title="<?php if(!empty($$t)) echo $$t; ?>">
                      <?php if(!empty($$v)) echo $$v; if(!empty($$t)) echo $qm; ?>
                    </label>
                    <select id="<?php echo $parameter; ?>" name="<?php echo $parameter; ?>" class="form-control">
                      <option <?php if(empty($$parameter)) echo 'selected'; ?> value="0">1) lazy load</option>
                      <option <?php if(!empty($$parameter)) echo 'selected'; ?> value="1">2) отложенная по времени загрузка</option>
                    </select>
                  </div>
                  <div class="panel-info"><?php echo_('text_vk_info'); ?>

                    <a target="_blank" href="https://vk.com/dev.php?f=2.1.%20%D0%9E%D0%B1%D1%8B%D1%87%D0%BD%D0%B0%D1%8F%20%D0%B8%D0%BD%D0%B8%D1%86%D0%B8%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F&method=openapi">
                      Обычная инициализация VK.init (стандартная)
                    </a>
                  </div>


                </div>
                <div class="tab-pane" id="facebook-panel" role="tabpanel" aria-labelledby="facebook-panel-tab">
                  <?php
                  checkbox_parameters(['facebook_optimize_off', 'facebook_optimize_sdk_doubles_off', 'facebook_pixel_off']);

                  ?>
                  <div class="panel-info"><?php echo_('text_facebook_info'); ?>
                    <p>
Виджеты Facebook реализуются за счет вставки кодов плагинов на страницу (в HTML). Примеры плагинов: <br>
                    <a target="_blank" href="https://developers.facebook.com/docs/plugins/page-plugin/">
                      Плагин «Страница»
                    </a>
                    <br>
                    <a target="_blank" href="https://developers.facebook.com/docs/plugins/embedded-posts">
                      "Встраиваемые публикации" ("Embedded Posts")
                    </a><br>
                    Код плагинов (виджетов) обычно вставляется таким образом как показано ниже.
                    Hi-Optimizer умеет оптимизировать виджеты, вставленные любым способом (через JS или iframe).
                    Виджеты, изначально вставленные через iframe, оптимизируются по общим правилам для iframe,
                      т.е. загружаются только когда попадают в область видимости.
                      В остальных случаях (инициализация через JS) виджет загружается только после загрузки всех наиболе важных файлов спустя определенное время
                      или после действия пользователя (клик, движение мышью, касание touchpad). Варианты "или" срабатывают в зависимости от того, что происходит раньше. </p><br><br>
                    <img src="view/image/sitecreator/hi-optimizer/info/sitecreator_ru_JVusi9gAju.jpg">
                  </div>
                </div>



                <div class="tab-pane" id="page_exclusion-panel" role="tabpanel" aria-labelledby="page_exclusion-panel-tab">
                  <?php
                  foreach ($textarea_parameters as $parameter) {
                    $t = 'tooltip_'. $parameter;
                    $v = 'text_'. $parameter;
                    ?>
                    <label style="margin-bottom: 10px;" class="form-check-label my_lable" for="<?php echo $parameter; ?>" data-toggle="tooltip" title="<?php if(!empty($$t)) echo $$t; ?>"><?php if(!empty($$v)) echo $$v; if(!empty($$t)) echo $qm; ?></label>
                    <div class="input-group">
                      <textarea style="margin-bottom: 20px;"  class="form-control" id="<?php echo $parameter; ?>" name="<?php echo $parameter; ?>"><?php if(!empty($$parameter)) echo $$parameter; ?></textarea>
                    </div>
                  <?php } ?>

                </div>
              </div>
            </form>

      <footer>
        <?php echo_('text_footer', $data); ?>
      </footer>
    </main>
  </div>

</div>

<script>
  function error_permission(error) {
    var danger = $('.alert.alert-danger');

    if(!danger.length) $('#main_nav nav.navbar').append('<div id="alert_danger" class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + error + '</div>');
    // if(!danger.length) $('#div_after_alert').before('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> ' + error + '</div>');
    else danger.html('<i class="fa fa-exclamation-circle"></i> ' + error);
    $('.alert-danger').slideDown().delay(3000).slideUp();
  }


  function wait_start(me) {
    if(Array.isArray(me)) {
      for(var i = 0; i < me.length; i++) {
        me[i].prop('disabled', true);
        me[i].children().prop('disabled', true);
      }
    }
    else {
      me.prop('disabled', true);
      me.children().prop('disabled', true);
    }

  }
  function wait_stop(me) {
    if(Array.isArray(me)) {
      for(var i = 0; i < me.length; i++) {
        me[i].prop('disabled', false);
        me[i].children().prop('disabled', false);
      }
    }
    else {
      me.prop('disabled', false);
      me.children().prop('disabled', false);
    }

  }


  function btns_wait_start() {
    var btn1 = $('#btn_moluleONorOFF');
    var btn2 = $('#clear_module_cache');
    var btn_box = $('#submit_btn_box');

    var btns = [btn1, btn2, btn_box];
    wait_start(btns);

  }

  function btns_wait_stop() {
    var btn1 = $('#btn_moluleONorOFF');
    var btn2 = $('#clear_module_cache');
    var btn_box = $('#submit_btn_box');

    var btns = [btn1, btn2, btn_box];
    wait_stop(btns);
  }

  $(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('.mytooltip').tooltip();
    $('.mytooltip').mouseleave(function (obj) {
// принудительно убираем фокус, иначе подсказка будет висеть (устраняем баг)
      $(obj.target).blur();

    });
  });


  function module_status() {
    var href = window.location.href;
    href = href.replace(/=(extension\/)?module\/hi_optimizer\b/i, '=module/hi_optimizer/module_status');
    $.ajax({
      url: href,
      type: 'post',
      dataType: 'json',
      success: function(json) {
        $('#module_status').html(json['text_module_status']);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        wait_stop(me);
      }
    });
  }

  function clear_ocmod(me) {
    <?php
    $oc15 = (version_compare(VERSION, "2.0", "<"))? true:false;
    ?>
    var oc15 = <?php echo ($oc15)? 'true': 'false'; ?>;
    if(oc15) return;  // opencart 1.5

    var clear_ocmod_url = '<?php if(!empty($clear_ocmod)) echo $clear_ocmod; ?>';
    $.ajax({
      url: clear_ocmod_url,
      dataType: 'html',
      beforeSend: function() {
        btns_wait_start();
      },
      success: function(content) {
        if (content) {
          var permission_error = '<?php if (!empty($modification_refresh_permission)) echo $modification_refresh_permission; ?>';
          if (permission_error != '') { //error
            alert("У Вас нет прав для управления модификаторами OCMOD ! \nYou do not have permission to manage OCMOD modifiers!");
          }
          else {
            module_status();
            console.log('ocmod cache is cleared');
            fun_clear_module_cache(); // очистим кеш после очередного включения/выключения
          }
        }
      },
      error: function(content) {
        alert(content);
        btns_wait_stop();
      }
    });

  }

  function fun_clear_module_cache() {
    var href = window.location.href;
    href = href.replace(/=(extension\/)?module\/hi_optimizer\b/i, '=module/hi_optimizer/clear_module_cache');

    $.ajax({
      url: href,
      type: 'post',
      dataType: 'json',
      beforeSend: function() {
        window.clear_module_cache_busy = true;
        btns_wait_start()
      },
      success: function(json) {
        if (json['error']) {
          error_permission(json['error']);
        }
        else {
          console.log('Module cache is cleared.');
          window.clear_module_cache_busy = false;
        }

        btns_wait_stop();
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        window.clear_module_cache_busy = false;
        btns_wait_stop();
      }

    });
  }

  // перед записью настроек очистим кеш модуля
  $('#btn_submit, #btn_submit_alt').on('click', function () {
    var me = this;
    fun_clear_module_cache();
    if(window.clear_module_cache_busy === false) return true;
    else {
      var tmr = setInterval(function () {
        if(window.clear_module_cache_busy === false) {
          clearTimeout(tmr);
          if(me.id === 'btn_submit_alt') $('input#noclose').val(1);
          $('#hi_optimizer_form').submit();
        }

      }, 100);
    }
    return false; // остановим кнопку

  });

  $('#clear_module_cache').on('click', function () {
    fun_clear_module_cache();
  });

  $('#btn_moluleONorOFF').on('click', function () {
    var me = $(this);
    var href = window.location.href;
    href = href.replace(/=(extension\/)?module\/hi_optimizer\b/i, '=module/hi_optimizer/moluleONorOFF');

    $.ajax({
      url: href,
      type: 'post',
      dataType: 'json',
      beforeSend: function() {
        // wait_start(me);
        btns_wait_start();
      },
      success: function(json) {
        if (json['error']) {
          error_permission(json['error']);
          btns_wait_stop();
        }
        else {
          clear_ocmod(me); // асинхронно, return будет до фактического завершения ajax

          if (json['btn_content']) {
            me.html(json['btn_content']);
          }
          if (json['flag_on']) {
            me.addClass('btn-outline-danger').removeClass('btn-outline-success');

          }
          else {
            me.addClass('btn-outline-success').removeClass('btn-outline-danger');
          }
        }


      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        // wait_stop(me);
        btns_wait_stop();
      }

    });
  });

  function del_script_for_optimization(i, el) {
    var id = 'script_added_' + i;
    console.log(id);
    $('#' + id).remove();
    $(el).remove();

  }

  $('#btn_add_script').on('click', function() {
    console.log('#btn_add_script click');
    console.log(window.script_added_count);
    if (window.script_added_count === undefined) window.script_added_count = 0;
    else window.script_added_count++;
    var end_el = $('#js-panel_end');
    var id = 'script_added_num_' + script_added_count;
    end_el.before('<br><button type="button" onclick="del_script_for_optimization(' + script_added_count + ', this);" class="btn btn-sm  btn-outline-secondary">удалить</button><textarea id="script_added_' + script_added_count + '" name="script_added[' + script_added_count + ']" class="form-control add_script"></textarea>');
  });

  $('#page_analysis_for_yandex_btn').on('click', function () {
    console.log('page_analysis_for_yandex_btn');
    var me = $(this);
    var href = window.location.href;
    href = href.replace(/=(extension\/)?module\/hi_optimizer\b/i, '=module/hi_optimizer/page_analysis_for_yandex');

    $.ajax({
      url: href,
      type: 'post',
      dataType: 'json',
      beforeSend: function() {
        wait_start(me);
      },
      success: function(json) {
        console.log(json['url']);
        // $('#your_website').html('<a target="_blank" href="'+ json['full_url']+ '">'+ json['url']+ '</a>');
        // $('#home_page_analysis_result').html(json['home_page_analysis_result']);
        // console.log(json['html']);
        if(json['full_url']) {
          $('#your_website_of_page_analysis_for_yandex').html('<a target="_blank" href="'+ json['full_url']+ '">'+ json['url']+ '</a>');
        }
        var output = $('#page_analysis_for_yandex_output');
        var n = json['html'].length;
        if(n) {
          output.html('');
          for (var i = 0; i < n; i++) {
            var pre = document.createElement("pre");
            $(pre).html('<code id="code_page_analysis_for_yandex_' + i + '">' + json['html'][i] + '</code>');
            var analyzed_block_name = document.createElement("div");
            var pos = json['position'][i];
            $(analyzed_block_name).addClass('analyzed_block_name').html(json['analyzed_block_name'][i] + ' <span># '+ (i+1) +'; </span><span class="code_start_position">Code start position (byte):<br>'+ pos +'</span>');
            output.append(analyzed_block_name);
            output.append($(pre));
          }
        }

        $('#page_analysis_for_yandex_output pre code').each(function(i, block) {
              hljs.highlightBlock(block);
              // hljs.lineNumbersBlock(block);
            });

        wait_stop(me);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        wait_stop(me);
      }
    });
  });

  $('#home_page_analysis_btn').on('click', function () {});

  function home_page_analysis(me) {
    console.log('home_page_analysis_btn');

    var href = window.location.href;
    href = href.replace(/=(extension\/)?module\/hi_optimizer\b/i, '=module/hi_optimizer/home_page_analysis');
    var uri = $('#page_uri_for_analysys_universal').val();
    uri = encodeURIComponent(uri);
    href = href + '&uri_parameter=' + uri;
    console.log('href', href);
    $.ajax({
      url: href,
      type: 'post',
      dataType: 'json',
      beforeSend: function() {
        wait_start(me);
      },
      success: function(json) {
        if (json['error']) {
          error_permission(json['error']);
        }
        else {
          console.log(json['url']);
          var max_length_txt = 150;
          var url_txt = json['url'];
          if(url_txt.length > max_length_txt) url_txt = url_txt.substr(0, max_length_txt) + '...';
          $('.your_website_link').html('<a target="_blank" href="'+ json['full_url']+ '">'+ url_txt+ '</a>');
          var title = json['title'];
          if(title.length > max_length_txt) title = title.substr(0, max_length_txt) + '...';
          $('.your_website_title').html(title);
          $('#home_page_analysis_result').html(json['home_page_analysis_result']);
          $('#code_home_page_analysis_result').html(json['home_page_analysis_result']);
          $('#code_home_page_analysis_result').each(function(i, block) {
            hljs.highlightBlock(block);
            hljs.lineNumbersBlock(block);
          });
        }


        wait_stop(me);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        wait_stop(me);
      }
    });

  }

  $(window).resize(function(){
    var h = parseInt($('#main_nav').outerHeight(true) + 1);
    var h2 = parseInt($('#nav_left_footer').outerHeight(true) + 1);
    var h_sum = h + h2;
    $('#left_sidebar').css('padding-top', h + 'px');
    var css_h = 'calc(100vh - ' + h_sum + 'px)';
    console.log(css_h);
    $('.sidebar-sticky').css('height', css_h);

  });
  $(window).trigger('resize');

  // $('#css_third_party_files_lowest_priority').prop('disabled', true);

  <?php
  $oc15 = (version_compare(VERSION, "2.0", "<"))? true:false;
  ?>
  var oc15 = <?php echo ($oc15)? 'true': 'false'; ?>;
  if(oc15) {
    $('#btn_moluleONorOFF').css('display', 'none');
  }

  function page_analysis_universal(type, me) {
    var href = window.location.href;
    href = href.replace(/=(extension\/)?module\/hi_optimizer\b/i, '=module/hi_optimizer/page_analysis_universal');
    var uri = $('#page_uri_for_analysys_universal').val();
    uri = encodeURIComponent(uri);
    href = href + '&type=' + type + '&uri_parameter=' + uri;

    $.ajax({
      url: href,
      type: 'post',
      dataType: 'json',
      beforeSend: function() {
        wait_start(me);
      },
      success: function(json) {
        if (json['error']) {
          error_permission(json['error']);
        }
        else {
          console.log(json['url']);
          var max_length_txt = 150;
          var url_txt = json['url'];
          if(url_txt.length > max_length_txt) url_txt = url_txt.substr(0, max_length_txt) + '...';
          $('.your_website_link').html('<a target="_blank" href="'+ json['full_url']+ '">'+ url_txt+ '</a>');
          var title = json['title'];
          if(title.length > max_length_txt) title = title.substr(0, max_length_txt) + '...';
          $('.your_website_title').html(title);

          var output = $('.tab-pane.active .page_analysis_output');
          var n = json['html'].length;
          if(n) {
            output.html('');
            for (var i = 0; i < n; i++) {
              var pre = document.createElement("pre");
              $(pre).html('<code id="code_page_analysis_for_yandex_' + i + '">' + json['html'][i] + '</code>');
              var analyzed_block_name = document.createElement("div");

              var name = '';
              if(json['analyzed_block_name']) name = json['analyzed_block_name'][i];
              var pos = '';
              if(json['position'][i]) {
                pos = json['position'][i];
                pos = '<span class="code_start_position">Code start position (byte):<br>'+ pos +'</span>';
              }

              $(analyzed_block_name).addClass('analyzed_block_name').html(name + ' <span># '+ (i+1) +'; </span>' + pos);
              output.append(analyzed_block_name);
              output.append($(pre));
            }
          }
          else {
            var t = new Date();
            output.html('<span class="page_analysis_universal_empty">Не найдено! ' + t + '</span>');
          }

          output.find('pre code').each(function(i, block) {
            hljs.highlightBlock(block);
            hljs.lineNumbersBlock(block);
          });

        }

        wait_stop(me);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        wait_stop(me);
      }
    });
  }


  $('#home_page_analysis_universal_btn').on('click', function (event) {
    var me = $(event.target);
    var id = $('.tab-pane.active').attr('id');
    console.log('home_page_analysis_universal_btn', id);
    switch(id) {
      default:
      case 'main-panel':
        $('#home_page_analysis-panel-tab').trigger('click');
      case 'home_page_analysis-panel':
        home_page_analysis(me);
        break;
      case 'js_link-panel':
        page_analysis_universal('js_link', me);
        break;


    }
  });

  $(document).ready(function () {
    var select = $('.page_uri_for_analysys');

    for(var i=0; i < page_uri__for_page_analisys.length; i++) {
      var option = page_uri__for_page_analisys[i];
      select.each(function(index){
        this.dataset.index = index;
        $(this).append("<option data-i='"+ i +"' value=\"" +option +"\">"+ option +"</option>");
      });
    }

    // select.on("change", function () {
    //   console.log(select.val());
    // });

    select.each(function(){
      $(this).on("change", function (event) {
        // console.log(event);
        var value = event.target.value;
        var index = event.target.dataset.index;
        var option_selected_i = $(event.target).find('option:selected').data('i');
        console.log(index, option_selected_i, value);

        $('.page_uri_for_analysys').each(function(){
          if(this.dataset.index === index) return;
          console.log('====', this);
          $(this).find('option').each(function(){
            console.log('----', this);
            console.log('option_selected_i', option_selected_i, 'this.dataset.i', this.dataset.i);
            if(Number(this.dataset.i) === Number(option_selected_i)) {
              $(this).prop('selected', true);
              }
            else $(this).prop('selected', false);
          });


          // $(this).find('option').removeAttr('selected');
          // $(this).find('option[data-i="'+option_selected_i+'"]').attr("selected", "selected");
        });

        $('.page_uri_for_analysys').each(function(i){
          console.log('*', i, this.value);
        });

      //   $('.page_uri_for_analysys').each(function(){
      //     if(this.id === id) return;
      //     $(this).find('option:selected').removeAttr('selected');
      //     $(this).find("option:contains('"+ value +"')").attr("selected", "selected");
      // });

        // $('.page_uri_for_analysys option:selected').each(function(){
        //   this.selected=false;
        // });
        // $('.page_uri_for_analysys').find("option:contains('"+ value +"')").each(function(){
        //   this.selected=true;
        // });
      });
    });

  });

</script>
</body></html>