<p>&nbsp;</p>
<p>Выполнение наименее важных скриптов имеет смысл отложить.<strong> Началом запуска таких скриптов служит <span style="color:#006400">действие пользователя</span> или <span style="color:#006400">истечение времени отсрочки</span> скрипта в зависимости от того какое событие произойдет раньше. </strong>Но в любом случае такая загрузка произойдет не ранее загрузки наиболее важного контента страницы.</p>

<p>Можно добавлять скрипты JS как внешние, т.е. загружаемые по ссылке (имеющие параметр <strong>src</strong>), так и встроенные (присутствующие в HTML) скрипты.&nbsp; Порядок добавления скриптов на этой вкладке не имеет значения, т.к. они будут обработаны строго в том порядке, в котором они расположены на странице.&nbsp;
  Порядок их выполнения также будет строго соблюден. Особенности выполнения асинхронных скриптов также учитываются.</p>

<p>Скрипты, которые добавлены на этой вкладке, будут иметь наименьший приоритет выполнения по сравнению с другими скриптами JS.&nbsp; Это означает, что они будут загружены и исполнены с отсрочкой по времени только после выполнения наиболее важных скриптов и после загрузки страницы и ее наиболее важных компонентов.&nbsp; Данные JS будут загружены и выполнены либо после действия пользователя (движение мышью, касание экрана смартфона, прокрутка и т.п.), либо спустя определенное время (например, 5 сек) после загрузки страницы и ее содержимого в зависимости от того какое событие наступит раньше (действие пользователя или истекание времени отсрочки).</p>

<p>Отсрочка выполнения некритичных скриптов позволяет ускорить загрузку страницы и ее наиболее важных компонентов. Некритичные - это, например, разнообразные счетчики, виджеты, чаты и т.п.&nbsp; <strong>Hi-Optimizer</strong> во многих случаях умеет сам детектировать разнообразные счетчики, метрики, аналитики, виджеты и т.п., поэтому нет нужды вносить на этой вкладке стандартные скрипты, которые известны <strong>Hi-Optimizer</strong>-у.&nbsp; Эта вкладка предназначена именно для нестандартных скриптов, например, та же Метрика от Яндекса может иметь код, который отличается от кода, который вам предоставил сам Яндекс, т.е. данный код намеренно был изменен, а потому есть риск, что <strong>Hi-Optimizer </strong>не может распознать его автоматически и, следовательно, оптимизировать автоматически. Следовательно, такой измененный код Метрики уже будет нестандартным, особенно если еще вырезаны комментарии, которые сопровождают начало и конец блока Метрики.</p>

<p>Вносить код нужно с открывающим и закрывающим тегами &lt;script&gt; и &lt;/script&gt;. Каждый отдельный блок (начинающийся и заканчивающийся &lt;script&gt; и &lt;/script&gt;) кода добавлять нужно отдельно. Код вносить нужно в точности в таком виде, в каком он присутствует в HTML, т.е. не допускать правок.
  Но есть исключение из правила: можно обрезать очень длинный код, главное при этом чтобы оставшаяся начальная часть кода оставалась уникальной и открывающий тег &lt;script&gt; присутствовал полностью.
  Блок кода JS можно вставлять также с предшествующим и/или последующим комментарием (в стиле HTML), которые есть в исходном коде HTML,
  важно чтобы вставляемый блок кода HTML (вместе с комментариями, скриптом JS) был взят непосредственного из исходного HTML и не был изменен. Исходный код HTML можно смотреть на вкладке "Анализ страниц сайта".
  Пример подобного кода показан ниже.</p>
<pre>
<code class="language-html">&lt;script src="catalog/view/javascript/option/option.js" type="text/javascript"&gt;&lt;/script&gt;</code></pre>
<pre>
<code class="language-html">&lt;script type="text/javascript" &gt;
     (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
       m[i].l=1*new Date();
      k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
     (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

     ym(55555647, "init", {
       clickmap:true,
       trackLinks:true,
       accurateTrackBounce:true,
       webvisor:true,
       ecommerce:"dataeeLayer"
     });
&lt;/script&gt;</code></pre>
<p>&nbsp;</p>
