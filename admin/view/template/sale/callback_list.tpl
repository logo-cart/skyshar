<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
       <div class="pull-right">
      <div class="buttons">
	  <a class="btn btn-primary" onclick="$('#form').attr('action', '<?php echo $update; ?>'); $('#form').submit();" class="button"><span  data-toggle="tooltip" title="<?php echo $status_done; ?>" ><i class="fa fa-refresh"></i></span></a>
	  <a class="btn btn-danger" onclick="$('form').submit();" class="button"><span><i class="fa fa-trash-o fa-fw"></i><?php echo $button_delete; ?></span></a>
		<a class="btn btn-primary" href="<?php echo $callback_setting ?>"><span><i class="fa fa-wrench fa-fw"></i><?php echo $button_callback_setting;?></span></a></div>
	</div>
      <h1><?php echo $heading_title_callback; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="alert alert-success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="content">
   
    <div class="container-fluid">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>

              <td class="left"><?php if ($sort == 'call_id') { ?>
                <a href="<?php echo $sort_call_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo "Номер"; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_call_id; ?>"><?php echo "Номер"; ?></a>
                <?php } ?>
				</td>
				
              <td class="left"><?php if ($sort == 'name') { ?>
                <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                <?php } ?></td>
             
			  <td class="right"><?php echo $text_callback_url; ?></td>
			  <td class="right"><?php echo $text_time_callback; ?></td>
			  <td class="right"><?php echo $text_topic_callback; ?></td>
			  <td class="right"><?php echo $text_comment; ?></td>
			  <td class="right"><?php if ($sort == 'username') { ?>
                <a href="<?php echo $sort_username; ?>" class="<?php echo strtolower($order); ?>"><?php echo $text_manager; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_username; ?>"><?php echo $text_manager; ?></a>
                <?php } ?></td>
			 
              <td class="right"><?php echo $text_status; ?></td>
              <td class="right"><?php echo $text_added; ?></td>
              <td class="right"><?php echo $text_modified; ?></td>
              <td class="right"><?php echo $text_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php if ($callbacks) { ?>
            <?php foreach ($callbacks as $callback) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($callback['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $callback['callback_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $callback['callback_id']; ?>" />
                <?php } ?></td>
				<td class="left"><?php echo $callback['callback_id']; ?></td>
				<td class="left">
					<?php if($callback['name']){?><div class="firstname"><i class="fa fa-user fa-fw"></i> <?php echo $callback['name']; ?></div><?php } ?>
					<?php if($callback['telephone']){?><div class="telephone"><i class="fa fa-phone fa-fw"></i><?php echo $callback['telephone']; ?></div><?php } ?>
					<?php if($callback['email_buyer']){?><div class="email"><i class="fa fa-envelope-o fa-fw"></i> <?php echo $callback['email_buyer']; ?></div><?php } ?>
					<?php if($callback['comment_buyer']){?><div class="email"><i class="fa fa fa-comment fa-fw"></i> <?php echo utf8_substr(strip_tags($callback['comment_buyer']),0,50); ?></div><?php } ?>
				</td>
				<td class="right"><a href="<?php echo $callback['url_site']; ?>" target="_blank"><?php echo $callback['url_site']; ?></td>
				<td class="right"><?php echo $callback['time_callback_on']; ?></br><?php echo $callback['time_callback_off']; ?></td>
				<td class="right"><?php echo $callback['topic_callback_send']; ?></td>
				<td class="left"><?php echo utf8_substr(strip_tags($callback['comment']),0,50); ?></td> 
				<td class="right"><?php echo $callback['username']; ?></td>
           <?php if ($callback['status'] == $status_done) { ?>
              <td class="right" style="background:#0BED0B;"><?php echo $callback['status']; ?></td>
            <?php } else { ?>
              <td class="right" style="background:#EDB40B;"><?php echo $callback['status']; ?></td>
            <?php } ?>
              <td class="right"><?php echo $callback['date_added']; ?></td>
             <td class="right"><?php echo $callback['date_modified']; ?></td>
              <td class="text-center"><a class="btn btn-primary" href="<?php echo $callback['action']; ?>" data-toggle="tooltip" title="<?php echo $text_edit; ?>"><i class="fa fa-pencil"></i></a></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="9"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
