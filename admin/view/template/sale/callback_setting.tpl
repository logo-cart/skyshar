<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
 <div class="page-header">
    <div class="container-fluid">
       <div class="pull-right">
      <div class="buttons">
		  <a class="btn btn-primary" onclick="$('#form').submit();" class="button"><span  data-toggle="tooltip" title="<?php echo $button_save; ?>" ><i class="fa fa-save"></i></span></a>
		  <a class="btn btn-default" onclick="location = '<?php echo $cancel; ?>';" class="button"><span  data-toggle="tooltip" title="<?php echo $button_cancel; ?>" ><i class="fa fa-reply"></i></span></a>
	  </div>
    </div>
      <h1><?php echo $heading_title_callback; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
   <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><?php echo $error_warning; ?></div>
  <?php } ?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_setting_module; ?></h3>
		</div>
	</div>
	<div class="panel-body">
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#tab-fields-setting" data-toggle="tab"><?php echo $tab_fields_setting; ?></a></li>
				<li><a href="#tab-general-setting" data-toggle="tab"><?php echo $tab_general_setting; ?></a></li>
				<li><a href="#tab-contact-setting" data-toggle="tab"><?php echo $tab_contact_setting; ?></a></li>
				<li><a href="#tab-design-setting" data-toggle="tab"><?php echo $tab_design_setting; ?></a></li>
				<li><a href="#tab-sms-setting" data-toggle="tab"><?php echo $tab_sms_setting; ?></a></li>
				<li><a href="#tab-email-setting" data-toggle="tab"><?php echo $tab_email_setting; ?></a></li>
			</ul>
			
		<div class="tab-content">
			<div class="tab-pane active" id="tab-fields-setting">
				<div class="form-group">					
					<div class="col-sm-2 title-fields-callback"><?php echo $text_on_off_fields_firstname;?></div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_status_fields;?></label>
					<div class="col-sm-10">
						<div class="onoffswitch">
							<input value="1" type="checkbox" name="config_on_off_fields_firstname_cb" class="onoffswitch-checkbox" id="config_on_off_fields_firstname_cb" <?php echo $config_on_off_fields_firstname_cb == '1' ? 'checked' : '' ; ?>>
							<label class="onoffswitch-label" for="config_on_off_fields_firstname_cb">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</div>
				</div>
			
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_requared_fields;?></label>
					<div class="col-sm-10">
						<div class="onoffswitch">
							<input value="1" type="checkbox" name="config_fields_firstname_requared_cb" class="onoffswitch-checkbox" id="config_fields_firstname_requared_cb" <?php echo $config_fields_firstname_requared_cb == '1' ? 'checked' : '' ; ?>>
							<label class="onoffswitch-label" for="config_fields_firstname_requared_cb">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_placeholder_fields; ?></label>
					<div class="col-sm-3">
						<?php foreach ($languages as $language) { ?>
							<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
								<input name="config_placeholder_fields_firstname_cb[<?php echo $language['language_id']; ?>][config_placeholder_fields_firstname_cb]" rows="5" class="form-control" value="<?php echo isset($config_placeholder_fields_firstname_cb[$language['language_id']]) ? $config_placeholder_fields_firstname_cb[$language['language_id']]['config_placeholder_fields_firstname_cb'] : ''; ?>"></input>
							</div>
						<?php } ?>
					</div>
				</div>
				
				<!--ПОЛЕ TELEPHONE-->
				<div class="form-group">					
					<div class="col-sm-2 title-fields-callback"><?php echo $text_on_off_fields_phone;?></div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_status_fields;?></label>
					<div class="col-sm-10">
						<div class="onoffswitch">
							<input value="1" type="checkbox" name="config_on_off_fields_phone_cb" class="onoffswitch-checkbox" id="config_on_off_fields_phone_cb" <?php echo $config_on_off_fields_phone_cb == '1' ? 'checked' : '' ; ?>>
							<label class="onoffswitch-label" for="config_on_off_fields_phone_cb">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_requared_fields;?></label>
						<div class="col-sm-10">
						<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_fields_phone_requared_cb" class="onoffswitch-checkbox" id="config_fields_phone_requared_cb" <?php echo $config_fields_phone_requared_cb == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_fields_phone_requared_cb">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_placeholder_fields; ?></label>
					<div class="col-sm-3">
						<?php foreach ($languages as $language) { ?>
							<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
								<input name="config_placeholder_fields_phone_cb[<?php echo $language['language_id']; ?>][config_placeholder_fields_phone_cb]" rows="5" class="form-control" value="<?php echo isset($config_placeholder_fields_phone_cb[$language['language_id']]) ? $config_placeholder_fields_phone_cb[$language['language_id']]['config_placeholder_fields_phone_cb'] : ''; ?>"></input>
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $entry_mask_phone_number?></label>
					<div class="col-sm-10">
						<input class="form-control" type="text" name="config_mask_phone_number_cb" value="<?php echo $config_mask_phone_number_cb; ?>" />
					</div>
				</div>
				<!--ПОЛЕ COMMENT-->
				<div class="form-group">					
					<div class="col-sm-2 title-fields-callback"><?php echo $text_on_off_fields_comment;?></div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_status_fields;?></label>
						<div class="col-sm-10">
						<div class="onoffswitch">
							<input value="1" type="checkbox" name="config_on_off_fields_comment_cb" class="onoffswitch-checkbox" id="config_on_off_fields_comment_cb" <?php echo $config_on_off_fields_comment_cb == '1' ? 'checked' : '' ; ?>>
							<label class="onoffswitch-label" for="config_on_off_fields_comment_cb">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_requared_fields;?></label>
					<div class="col-sm-10">
						<div class="onoffswitch">
							<input value="1" type="checkbox" name="config_fields_comment_requared_cb" class="onoffswitch-checkbox" id="config_fields_comment_requared_cb" <?php echo $config_fields_comment_requared_cb == '1' ? 'checked' : '' ; ?>>
							<label class="onoffswitch-label" for="config_fields_comment_requared_cb">
								<span class="onoffswitch-inner"></span>
								<span class="onoffswitch-switch"></span>
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_placeholder_fields; ?></label>
					<div class="col-sm-3">
						<?php foreach ($languages as $language) { ?>
							<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
								<input name="config_placeholder_fields_comment_cb[<?php echo $language['language_id']; ?>][config_placeholder_fields_comment_cb]" rows="5" class="form-control" value="<?php echo isset($config_placeholder_fields_comment_cb[$language['language_id']]) ? $config_placeholder_fields_comment_cb[$language['language_id']]['config_placeholder_fields_comment_cb'] : ''; ?>"></input>
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="clearfix"></div>	
				<!--ПОЛЕ EMAIL-->
				<div class="form-group">					
					<div class="col-sm-2 title-fields-callback"><?php echo $text_on_off_fields_email;?></div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_status_fields;?></label>
					<div class="col-sm-10">
						<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_on_off_fields_email_cb" class="onoffswitch-checkbox" id="config_on_off_fields_email_cb" <?php echo $config_on_off_fields_email_cb == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_on_off_fields_email_cb">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_requared_fields;?></label>
					<div class="col-sm-10">
						<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_fields_email_requared_cb" class="onoffswitch-checkbox" id="config_fields_email_requared_cb" <?php echo $config_fields_email_requared_cb == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_fields_email_requared_cb">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_placeholder_fields; ?></label>
					<div class="col-sm-3">
					<?php foreach ($languages as $language) { ?>
							<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
								<input name="config_placeholder_fields_email_cb[<?php echo $language['language_id']; ?>][config_placeholder_fields_email_cb]" rows="5" class="form-control" value="<?php echo isset($config_placeholder_fields_email_cb[$language['language_id']]) ? $config_placeholder_fields_email_cb[$language['language_id']]['config_placeholder_fields_email_cb'] : ''; ?>"></input>
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
			
			
			<div class="tab-pane" id="tab-general-setting">
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_on_off_date_time;?></label>
					<div class="col-sm-3">
						<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_on_off_date_time" class="onoffswitch-checkbox" id="config_on_off_date_time" <?php echo $config_on_off_date_time == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_on_off_date_time">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $entry_title_callback_sendthis; ?></label>
					<div class="col-sm-3">
					<?php foreach ($languages as $language) { ?>
							<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
								<input name="config_title_callback_sendthis[<?php echo $language['language_id']; ?>][config_title_callback_sendthis]" rows="5" class="form-control" value="<?php echo isset($config_title_callback_sendthis[$language['language_id']]) ? $config_title_callback_sendthis[$language['language_id']]['config_title_callback_sendthis'] : ''; ?>"></input>
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $entry_any_text_bottom_before_button; ?></label>
					<div class="col-sm-3">
					<?php foreach ($languages as $language) { ?>
							<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
								<input name="config_any_text_bottom_before_button[<?php echo $language['language_id']; ?>][config_any_text_bottom_before_button]" rows="5" class="form-control" value="<?php echo isset($config_any_text_bottom_before_button[$language['language_id']]) ? $config_any_text_bottom_before_button[$language['language_id']]['config_any_text_bottom_before_button'] : ''; ?>"></input>
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $entry_img_left_callback ?></label>
					<div class="col-sm-3">
						<a href="" id="thumb-imgleft" data-toggle="image" class="img-thumbnail"><img src="<?php echo $imgleft; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
						<input type="hidden" name="config_img_left" value="<?php echo $config_img_left; ?>" id="input-imgleft" />
					</div>
				</div>			
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $entry_background_callback ?></label>
					<div class="col-sm-10">
						<input class="form-control jpicker-csseditor-callback_20" type="text" name="config_background_callback" value="<?php echo $config_background_callback ?>"  />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $entry_background_callback_hover ?></label>
					<div class="col-sm-10">
						<input class="form-control jpicker-csseditor-callback_20" type="text" name="config_background_callback_hover" value="<?php echo $config_background_callback_hover ?>"  />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $entry_background_button_callback ?></label>
					<div class="col-sm-10">
						<input class="form-control jpicker-csseditor-callback_20" type="text" name="config_background_button_callback" value="<?php echo $config_background_button_callback ?>"  />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $entry_background_button_callback_hover ?></label>
					<div class="col-sm-10">
						<input class="form-control jpicker-csseditor-callback_20" type="text" name="config_background_button_callback_hover" value="<?php echo $config_background_button_callback_hover ?>"  />
					</div>
				</div>
				
		</div>
			<div class="tab-pane" id="tab-contact-setting">
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_on_off_contact_right;?></label>
					<div class="col-sm-10">
						<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_on_off_contact_right" class="onoffswitch-checkbox" id="config_on_off_contact_right" <?php echo $config_on_off_contact_right == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_on_off_contact_right">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $entry_title_mob; ?></label>
					<div class="col-sm-3">
					<?php foreach ($languages as $language) { ?>
							<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
								<input name="block_name_phone[<?php echo $language['language_id']; ?>][block_name_phone]" rows="5" class="form-control" value="<?php echo isset($block_name_phone[$language['language_id']]) ? $block_name_phone[$language['language_id']]['block_name_phone'] : ''; ?>"></input>
							</div>
						<?php } ?>
					</div>
				</div>
				 <?php if($config_phones) { ?>
				  <?php $phone_row = 1;?>
					<?php foreach($config_phones_array as $config_phone) { ?>
					<div class="form-group" id="phone_row<?php echo $phone_row;?>">						
						<label class="col-sm-2 control-label"><?php echo $entry_mob ?><?php echo $phone_row;?></label>
						<div class="col-sm-2">
							<a href="" id="thumb-image-phone<?php echo $phone_row;?>" data-toggle="image" class="img-thumbnail">
							<img src="<?php echo $config_phone['icon_phone']; ?>" alt=""/></a>
							<input type="hidden" name="config_phones[<?php echo $phone_row; ?>][image]" value="<?php echo $config_phone['image']; ?>" id="input-image-phone-<?php echo $phone_row;?>" />
						</div>
						<div class="col-sm-2">
							<input class="form-control" type="text"  name="config_phones[<?php echo $phone_row; ?>][phone]" value="<?php echo $config_phone['phone']; ?>"/>
						</div>
						<div class="col-sm-2">
							<a class="btn btn-danger" onclick="$('#phone_row<?php echo $phone_row;?>').remove();" class="button"><?php echo $text_delete; ?></a>
						</div>						
					</div>
						<?php $phone_row++;?>	
					<?php } ?>
					
				<?php } else { ?>
					<?php $phone_row = 1;?>
				<?php } ?>
				<div class="form-group" id="phone">
					<label class="col-sm-2 control-label"><?php echo $entry_mob ?></label>
					<div class="col-sm-2">
						<a onclick="addPhone();" class="btn btn-primary"><i class="fa fa-plus-circle fa-fw"></i><?php echo $text_add; ?></a></td>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $entry_email ?></label>
					<div class="col-sm-3">
						<input class="form-control" type="text" name="config_email_1" value="<?php echo $config_email_1 ?>"  />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $entry_skype ?></label>
					<div class="col-sm-3">
						<input class="form-control" type="text" name="config_skype" value="<?php echo $config_skype ?>"  />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $time_on_status_skype ?></label>
					<div class="col-sm-3 input-group time">
						<input type="text" class="form-control" name="config_skype_date_start" value="<?php echo $config_skype_date_start; ?>"  data-date-format="HH:mm"/>
						<span class="input-group-btn">
							<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
						</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $time_off_status_skype ?></label>
					<div class="col-sm-3 input-group time">
						<input type="text" class="form-control" name="config_skype_date_end" value="<?php echo $config_skype_date_end; ?>"  data-date-format="HH:mm"/>
						<span class="input-group-btn">
							<button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
						</span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $on_off_status_skype;?></label>
					<div class="col-sm-3">
						<div class="onoffswitch">
						<input value="1" type="checkbox" name="delete_status_skype_from_site" class="onoffswitch-checkbox" id="delete_status_skype_from_site" <?php echo $delete_status_skype_from_site == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="delete_status_skype_from_site">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $entry_title_schedule; ?></label>
					<div class="col-sm-3">
					<?php foreach ($languages as $language) { ?>
							<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
								<input name="config_title_schedule[<?php echo $language['language_id']; ?>][config_title_schedule]" rows="5" class="form-control" value="<?php echo isset($config_title_schedule[$language['language_id']]) ? $config_title_schedule[$language['language_id']]['config_title_schedule'] : ''; ?>"></input>
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $entry_daily; ?></label>
					<div class="col-sm-3">
					<?php foreach ($languages as $language) { ?>
							<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
								<input name="config_daily[<?php echo $language['language_id']; ?>][config_daily]" rows="5" class="form-control" value="<?php echo isset($config_daily[$language['language_id']]) ? $config_daily[$language['language_id']]['config_daily'] : ''; ?>"></input>
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $entry_weekend; ?></label>
					<div class="col-sm-3">
					<?php foreach ($languages as $language) { ?>
							<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
								<input name="config_weekend[<?php echo $language['language_id']; ?>][config_weekend]" rows="5" class="form-control" value="<?php echo isset($config_weekend[$language['language_id']]) ? $config_weekend[$language['language_id']]['config_weekend'] : ''; ?>"></input>
							</div>
						<?php } ?>
					</div>
				</div>
				<?php $call_topic = 1;?>
				<?php foreach($call_topic_array as $result_call_topic) { ?>				
					<div class="form-group" id="call_topic<?php echo $call_topic;?>">	
					<label class="col-sm-2 control-label"><?php echo "№ - " ?><?php echo $call_topic;?></label>					
					<input type="hidden" name="call_topic[<?php echo $call_topic; ?>][id]" value="<?php echo $result_call_topic['id']; ?>"></input>					
						<div class="col-sm-2">
							<input type="text" name="call_topic[<?php echo $call_topic; ?>][name]" value="<?php echo $result_call_topic['name']; ?>"></input>
						</div>
						<div class="col-sm-2">
							<a class="btn btn-danger" onclick="$('#call_topic<?php echo $call_topic;?>').remove();deletesubjectcall('<?php echo $result_call_topic['id']; ?>');" class="button"><?php echo $text_delete; ?></a>
						</div>						
					</div>					
				<?php $call_topic++;?>	
				<?php } ?>				
				<div class="form-group" id="call_topic">
					<label class="col-sm-2 control-label"><?php echo $text_theme_callback; ?></label>
					<div class="col-sm-2">
						<button type="button" onclick="addCalltopic();" data-toggle="tooltip" title="<?php echo $button_image_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle fa-fw"></i><?php echo $text_add; ?></button>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $entry_social_block_title; ?></label>
					<div class="col-sm-3">
					<?php foreach ($languages as $language) { ?>
							<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
								<input name="config_social_block_title[<?php echo $language['language_id']; ?>][config_social_block_title]" rows="5" class="form-control" value="<?php echo isset($config_social_block_title[$language['language_id']]) ? $config_social_block_title[$language['language_id']]['config_social_block_title'] : ''; ?>"></input>
							</div>
						<?php } ?>
					</div>
				</div>		
				<?php $image_row = 1;?>
				<?php foreach($product_images as $product_image) { ?>				
					<div class="form-group" id="image-row<?php echo $image_row; ?>">	
					<label class="col-sm-2 control-label"><?php echo "№ - " ?><?php echo $image_row;?></label>					
					<input type="hidden" name="social[<?php echo $image_row; ?>][social_id]" value="<?php echo $product_image['social_id']; ?>"></input>				
						<div class="col-sm-2">
							<a href="" id="thumb-image<?php echo $image_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $product_image['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
							<input type="hidden" name="social[<?php echo $image_row; ?>][image]" value="<?php echo $product_image['image']; ?>" id="input-image<?php echo $image_row; ?>" />
						</div>
						<div class="col-sm-2">
							<input type="text" name="social[<?php echo $image_row; ?>][name]" value="<?php echo $product_image['name']; ?>"  class="form-control" />
						</div>	
						<div class="col-sm-2">
							<a class="btn btn-danger" onclick="$('#image-row<?php echo $image_row;?>').remove();deleteSocial('<?php echo $product_image['social_id']; ?>');" class="button"><?php echo $text_delete; ?></a>
						</div>						
					</div>					
				<?php $image_row++;?>	
				<?php } ?>					
					<div class="form-group" id="social">
						<label class="col-sm-2 control-label"><?php echo $entry_social_callback; ?></label>
						<div class="col-sm-2">
							<button type="button" onclick="addImage();" data-toggle="tooltip" title="<?php echo $button_image_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle fa-fw"></i><?php echo $text_add; ?></button>
						</div>
					</div>
<script type="text/javascript"><!--
function deleteSocial(social_id) {	
	$.post('index.php?route=sale/callback/deleteSocial&token=<?php echo $token; ?>', 'social_id=' + social_id);
}
function deletesubjectcall(id) {	
	$.post('index.php?route=sale/callback/deletesubjectcall&token=<?php echo $token; ?>', 'call_topic_id=' + id);
}

var image_row = <?php echo $image_row; ?>;

function addImage() {
	html  = '<div class="form-group" id="image-row'+ image_row +'">';
	html += '  <label class="col-sm-2 control-label"><?php echo "№ - " ?>'+ image_row+'</label>	';
	html += '  <div class="col-sm-2">';
	html += '  		<a href="" id="thumb-image' + image_row + '"data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /><input type="hidden" name="new_social[' + image_row + '][image]" value="" id="input-image' + image_row + '" /></a>';
	html += '  </div>';
	html += '  <div class="col-sm-2">';
	html += '  		<input type="text" name="new_social[' + image_row + '][name]" value="" class="form-control" />';
	html += '  </div>';
	html += '  <div class="col-sm-2">';
	html += '  		<button type="button" onclick="$(\'#image-row' + image_row  + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><?php echo $text_delete; ?></button>';
	html += '  </div>';
	html += '</div>';
	
	$('#social').before(html);
	
	image_row++;
}
//--></script>		
<script type="text/javascript"><!--
var call_topic = <?php echo $call_topic; ?>;
function addCalltopic() {
	html  = '<div class="form-group" id="call_topic'+ call_topic +'">';
	html += '  <label class="col-sm-2 control-label"><?php echo "Тема звонка № - " ?>'+ call_topic +'</label>';
	html += '  <div class="col-sm-2">';
	html += '     <input type="text" name="new_call_topic['+ call_topic +'][name]" value="" />';
	html += '  </div>';
	html += '  <div class="col-sm-2">';
	html += '  		<button type="button" onclick="$(\'#call_topic' + call_topic + '\').remove();" data-toggle="tooltip" title="<?php echo $text_delete; ?>" class="btn btn-danger"><?php echo $text_delete; ?></button>';
	html += '  </div>';
	html += '</div>';
	
	$('#call_topic').before(html);
	
	call_topic++;
}
//--></script> 			
<script type="text/javascript">
	$('.time').datetimepicker({
	pickDate: false
});
</script>			
<script type="text/javascript"><!--			
var phone_row = <?php echo $phone_row;?>;
function addPhone() {
	html  = '<div class="form-group" id="phone_row' + phone_row + '">';
	html += '  <label class="col-sm-2 control-label"><?php echo $entry_mob ?>'+ phone_row +'</label>';
	html += '  <div class="col-sm-2">';
	html += '  		<a href="" id="thumb-image-phone'+ phone_row +'" data-toggle="image" class="img-thumbnail">';
	html += '  		<img src="<?php echo $no_image_phone;?>" alt=""/></a>';
	html += '  <input type="hidden" name="config_phones['+ phone_row +'][image]" value="" id="input-image-phone-'+ phone_row +'" />';
	html += '  </div>';
	html += '  <div class="col-sm-2">';
	html += '  <input class="form-control" type="text"  name="config_phones['+ phone_row +'][phone]" value=""/>';
	html += '  </div>';
	html += '  <div class="col-sm-2">';
	html += ' <a class="btn btn-danger" onclick="$(\'#phone_row' + phone_row + '\').remove();" class="button"><?php echo $text_delete; ?></a>';
	html += '  </div>';
	html += '</div>';	
	$('#phone').before(html);	
	phone_row++;
}
//--></script>			
			</div>
			
			
			<div class="tab-pane" id="tab-design-setting">
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_select_design_theme_callback;?></label>
					<div class="col-sm-3">					
						<select class="form-control select_design_theme_callback" onchange="sel(this.value);" name="select_design_theme_callback">							
								<?php if ($select_design_theme_callback == 1) { ?>
									<option value="1" selected="selected"><?php echo '№ - 1';?></option>
								<?php } else { ?>
									<option value="1"><?php echo '№ - 1';?></option>
								<?php } ?>
								<?php if ($select_design_theme_callback == 2) { ?>
									<option value="2" selected="selected"><?php echo '№ - 2';?></option>
								<?php } else { ?>
									<option value="2" ><?php echo '№ - 2';?></option>
								<?php } ?>								
						</select>
					</div>
				</div>
				<script>
				$(window).load(function(){ 
						var select_design_theme_callback = $('.select_design_theme_callback option:selected').val();						
						if (select_design_theme_callback =='2') {
							$('.rightorleft').toggleClass('active');
						} else if (select_design_theme_callback =='1') {
							$('.rightorleft').removeClass('active');
						}
				})
				function sel(select_design_theme_callback){				
						if (select_design_theme_callback =='2') {
							$('.rightorleft').addClass('active');
						} else if (select_design_theme_callback =='1') {
							$('.rightorleft').removeClass('active');
						}
					}
				</script>
				<style>
					.rightorleft {
						display:none;
					}
					.rightorleft.active {
						display:block;
					}
				</style>
				<div class="form-group rightorleft">
					<label class="col-sm-2 control-label"><?php echo $entry_position_callback;?></label>
					<div class="col-sm-3">					
						<select class="form-control" name="select_design_theme_callback_left_or_right">							
								
								<?php if ($select_design_theme_callback_left_or_right == 1) { ?>
									<option value="1" selected="selected"><?php echo $text_theme2_right;?></option>
								<?php } else { ?>
									<option value="1"><?php echo $text_theme2_right;;?></option>
								<?php } ?>
								<?php if ($select_design_theme_callback_left_or_right == 2) { ?>
									<option value="2" selected="selected"><?php echo $text_theme2_left;;?></option>
								<?php } else { ?>
									<option value="2"><?php echo $text_theme2_left;;?></option>
								<?php } ?>								
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo '№ - 1';?></label>
					<div class="col-sm-3">	
						<img class="responsive" src="view/image/theme1.png">	
					</div>
					<label class="col-sm-2 control-label"><?php echo '№ - 2';?></label>
					<div class="col-sm-3">	
						<img class="responsive" src="view/image/theme2.png">						
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_right_callback_position_fixed;?></label>
					<div class="col-sm-3">
						<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_right_callback_position_fixed" class="onoffswitch-checkbox" id="config_right_callback_position_fixed" <?php echo $config_right_callback_position_fixed == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_right_callback_position_fixed">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_right_callback_position_fixed_new_btn;?></label>
					<div class="col-sm-1">
						<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_right_callback_position_fixed_new_btn" class="onoffswitch-checkbox" id="config_right_callback_position_fixed_new_btn" <?php echo $config_right_callback_position_fixed_new_btn == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_right_callback_position_fixed_new_btn">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
						</div>
					</div>
					<div class="col-sm-3">
						<select class="form-control" name="config_right_callback_position_fixed_new_btn_right">
								<option><?php echo $entry_position_callback;?></option>									
								<?php if ($config_right_callback_position_fixed_new_btn_right == 1) { ?>
									<option value="1" selected="selected"><?php echo $text_position_top_left;?></option>
								<?php } else { ?>
									<option value="1"><?php echo $text_position_top_left;?></option>
								<?php } ?>
								<?php if ($config_right_callback_position_fixed_new_btn_right == 2) { ?>
									<option value="2" selected="selected"><?php echo $text_position_center_left;?></option>
								<?php } else { ?>
									<option value="2"><?php echo $text_position_center_left;?></option>
								<?php } ?>
								<?php if ($config_right_callback_position_fixed_new_btn_right == 3) { ?>
									<option value="3" selected="selected"><?php echo $text_position_bottom_left;?></option>
								<?php } else { ?>
									<option value="3"><?php echo $text_position_bottom_left;?></option>
								<?php } ?>
								<?php if ($config_right_callback_position_fixed_new_btn_right == 4) { ?>
									<option value="4" selected="selected"><?php echo $text_position_top_right;?></option>
								<?php } else { ?>
									<option value="4"><?php echo $text_position_top_right;?></option>
								<?php } ?>
								<?php if ($config_right_callback_position_fixed_new_btn_right == 5) { ?>
									<option value="5" selected="selected"><?php echo $text_position_center_right;?></option>
								<?php } else { ?>
									<option value="5"><?php echo $text_position_center_right;?></option>
								<?php } ?>
								<?php if ($config_right_callback_position_fixed_new_btn_right == 6) { ?>
									<option value="6" selected="selected"><?php echo $text_position_bottom_right;?></option>
								<?php } else { ?>
									<option value="6"><?php echo $text_position_bottom_right;?></option>
								<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $text_callback_position_fixed_new_btn2;?></label>
					<div class="col-sm-1">
						<div class="onoffswitch">
						<input value="1" type="checkbox" name="config_on_off_new_btn2" class="onoffswitch-checkbox" id="config_on_off_new_btn2" <?php echo $config_on_off_new_btn2 == '1' ? 'checked' : '' ; ?>>
						<label class="onoffswitch-label" for="config_on_off_new_btn2">
							<span class="onoffswitch-inner"></span>
							<span class="onoffswitch-switch"></span>
						</label>
						</div>
					</div>
					<div class="col-sm-3">
						<select class="form-control" name="config_callback_position_fixed_new_btn2">
								<option><?php echo $entry_position_callback;?></option>									
								<?php if ($config_callback_position_fixed_new_btn2 == 1) { ?>
									<option value="1" selected="selected"><?php echo $text_position_top_left;?></option>
								<?php } else { ?>
									<option value="1"><?php echo $text_position_top_left;?></option>
								<?php } ?>
								<?php if ($config_callback_position_fixed_new_btn2 == 2) { ?>
									<option value="2" selected="selected"><?php echo $text_position_center_left;?></option>
								<?php } else { ?>
									<option value="2"><?php echo $text_position_center_left;?></option>
								<?php } ?>
								<?php if ($config_callback_position_fixed_new_btn2 == 3) { ?>
									<option value="3" selected="selected"><?php echo $text_position_bottom_left;?></option>
								<?php } else { ?>
									<option value="3"><?php echo $text_position_bottom_left;?></option>
								<?php } ?>
								<?php if ($config_callback_position_fixed_new_btn2 == 4) { ?>
									<option value="4" selected="selected"><?php echo $text_position_top_right;?></option>
								<?php } else { ?>
									<option value="4"><?php echo $text_position_top_right;?></option>
								<?php } ?>
								<?php if ($config_callback_position_fixed_new_btn2 == 5) { ?>
									<option value="5" selected="selected"><?php echo $text_position_center_right;?></option>
								<?php } else { ?>
									<option value="5"><?php echo $text_position_center_right;?></option>
								<?php } ?>
								<?php if ($config_callback_position_fixed_new_btn2 == 6) { ?>
									<option value="6" selected="selected"><?php echo $text_position_bottom_right;?></option>
								<?php } else { ?>
									<option value="6"><?php echo $text_position_bottom_right;?></option>
								<?php } ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-1 control-label" for="register_site"><?php echo "№ - 1"; ?></label>
						<div class="col-sm-3">
						<div id="tcb-call_1">
							<div class="tcb-phone_1">
								<div class="tcb-phone-img_1"></div>
							</div>
							<div id="takecallback_1" class="tcb-layout_1"></div>
						</div>
						</div>
					<label class="col-sm-1 control-label" for="register_site"><?php echo "№ - 2"; ?></label>
						<div class="col-sm-3">
							<div id="tcb-call">
								<div class="tcb-phone">
									<div class="tcb-phone-img"></div>
								</div>
								<div id="takecallback" class="tcb-layout"></div>
							</div>
						</div>
				</div>
			</div>
			<div class="tab-pane" id="tab-sms-setting">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="register_site"><?php echo $register_site; ?></label>
						<div class="col-sm-10">
							<div><a href="http://my.smscab.ru/"><?php echo "http://my.smscab.ru/";?></a></div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo $on_off_sms_callback ; ?></label>
						<div class="col-sm-10">
							<div class="onoffswitch">
								<input value="1" type="checkbox" name="config_send_sms_on_off" class="onoffswitch-checkbox" id="config_send_sms_on_off" <?php echo $config_send_sms_on_off == '1' ? 'checked' : '' ; ?>>
								<label class="onoffswitch-label" for="config_send_sms_on_off">
									<span class="onoffswitch-inner"></span>
									<span class="onoffswitch-switch"></span>
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo $entry_phone_number_send_sms ?></label>
						<div class="col-sm-6">
							<input class="form-control" type="text" name="config_phone_number_send_sms" value="<?php echo $config_phone_number_send_sms ?>"  />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo $entry_login_send_sms ?></label>
						<div class="col-sm-6">
							<input class="form-control" type="text" name="config_login_send_sms" value="<?php echo $config_login_send_sms ?>"  />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo $entry_pass_send_sms ?></label>
						<div class="col-sm-6">
							<input class="form-control" type="text" name="config_pass_send_sms" value="<?php echo $config_pass_send_sms ?>"  />
						</div>
					</div>				
			</div>
			
			<div class="tab-pane" id="tab-email-setting">
				<div>
						<div class="form-group">					
								<div class="col-sm-5 title-fields-callback"><?php echo $form_latter_from_me_callback;?></div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label"><?php echo $text_on_off_send_me_mail_callback;?></label>
							<div class="col-sm-10">
								<div class="onoffswitch">
									<input value="1" type="checkbox" name="config_on_off_send_me_mail_callback" class="onoffswitch-checkbox" id="config_on_off_send_me_mail_callback" <?php echo $config_on_off_send_me_mail_callback == '1' ? 'checked' : '' ; ?>>
									<label class="onoffswitch-label" for="config_on_off_send_me_mail_callback">
										<span class="onoffswitch-inner"></span>
										<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</div>
						</div>
						<ul class="nav nav-tabs" id="language_me">
							<?php foreach ($languages as $language) { ?>
							<li><a href="#language_me<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
							<?php } ?>
						</ul>
							<div class="tab-content">
								<?php foreach ($languages as $language) { ?>
									<div class="tab-pane" id="language_me<?php echo $language['language_id']; ?>">
										<div class="form-group">
											<label class="col-sm-2 control-label" for="quickorder_subject_me_callback<?php echo $language['language_id']; ?>"><?php echo $callback_subject;?><br /><?php echo $subject_text_variables;?></label>
											<div class="col-sm-10">
												<textarea name="quickorder_subject_me_callback<?php echo $language['language_id'] ?>" id="quickorder_subject_me_callback<?php echo $language['language_id'] ?>" cols="50" rows="3"><?php echo isset(${'quickorder_subject_me_callback' . $language['language_id']}) ? ${'quickorder_subject_me_callback' . $language['language_id']} : ''; ?></textarea>   
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label" for="quickorder_description_me_callback<?php echo $language['language_id']; ?>"><?php echo $quickorder_description_me;?><br /><?php echo $subject_text_variables;?><br /><?php echo $list_of_variables_entry;?></label>
											<div class="col-sm-10">
												<textarea name="quickorder_description_me_callback<?php echo $language['language_id'] ?>" id="quickorder_description_me_callback<?php echo $language['language_id'] ?>" cols="50" rows="3"><?php echo isset(${'quickorder_description_me_callback' . $language['language_id']}) ? ${'quickorder_description_me_callback' . $language['language_id']} : ''; ?></textarea>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
						<div class="form-group">
						<label class="col-sm-2 control-label"><?php echo $entry_you_email_callback?></label>
						<div class="col-sm-10">
							<input class="form-control" type="text" name="config_you_email_callback" value="<?php echo $config_you_email_callback; ?>" />
						</div>
					</div>
					</div>
			</div>
	</div>
		</form>
	</div>
  
  
  
</div>
 <script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
$('#quickorder_description_me_callback<?php echo $language['language_id'] ?>').summernote({height: 300});
<?php } ?>
//--></script>
 <script type="text/javascript"><!--
$('#language_me a:first').tab('show');
//--></script>
<?php echo $footer; ?>
