<?php
ini_set("display_errors",1);
error_reporting(E_ALL);
  $url = $_SERVER['REQUEST_URI'];
  if (!strpos($url, 'route=') && !preg_match('/[\?]/', $url)) {
    $test_url = str_replace('index.php/','',$url);
	if($test_url != $url) {
    header('Location: ' . $test_url, TRUE, 301);
    exit(); 
  }
  }

 if ( preg_match('/[A-Z]/', $url) && !strpos($url, 'route=') && !preg_match('/[\?]/', $url)) {
    // Convert URL to lowercase
    $lc_url = strtolower($url);
    // 301 redirect to new lowercase URL
    header('Location: ' . $lc_url, TRUE, 301);
    exit(); 
  } 
//include 'redirect301map.php';
  
$uri = preg_replace("/\?.*/i",'', $_SERVER['REQUEST_URI']);
if (strlen($uri)>1) {
  if (rtrim($uri,'/')!=$uri) {
    header("HTTP/1.1 301 Moved Permanently");
    header('Location: https://'.$_SERVER['SERVER_NAME'].str_replace($uri, rtrim($uri,'/'), $_SERVER['REQUEST_URI']));
    exit();    
  }
}



// Version
define('VERSION', '3.0.3.2');

// Configuration
if (is_file('config.php')) {
	require_once('config.php');
}

// Install
if (!defined('DIR_APPLICATION')) {
	header('Location: install/index.php');
	exit;
}

// Startup
require_once(DIR_SYSTEM . 'startup.php');

start('catalog');