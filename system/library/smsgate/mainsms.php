<?php
//	@copyright	OC-HELP.com
//	@website	https://oc-help.com
//	@support	support@oc-help.com
// 	@developer	Alexander Vakhovskiy

final class Mainsms extends SmsGate {
	public function send() {
		//Sms Log
		$sms_log = new Log('sms_log.log');

		if ($this->username) {
			if ($this->to && $this->copy) {
				$numbers = $this->prepPhone($this->to) . ',' . $this->prepPhone($this->copy);
			} elseif ($this->to) {
				$numbers = $this->prepPhone($this->to);
			} else {
				$numbers = false;
				$sms_log->write('(Mainsms.ru) Error: Phone destination not found!');
			}

			if ($this->from) {
				$sender = $this->from;
			} else {
				$sender = $this->from;
				$sms_log->write('(Mainsms.ru) Notice: Default Sender set! Please input real Sender');
			}

			$params = array(
				'recipients' => $numbers,
				'message' => $this->message,
				'sender' => $sender,
				// 'test' => '1',
			);

			$params = $this->joinArrayValues($params);
			$sign = $this->generateSign($params);

			$params = array_merge(array('project' => $this->username), $params);

			$post_data = http_build_query(array_merge($params, array('sign' => $sign)), '', '&');

			$balance = file_get_contents("https://mainsms.ru/api/mainsms/message/balance?" . $post_data);

			$balance_result = json_decode($balance);

			if ($balance_result->balance && $numbers) {

				$sms_log->write('(Mainsms.ru) Balance: Status = ' . $balance_result->status . ' Balance: ' . $balance_result->balance);

				$result = file_get_contents("https://mainsms.ru/api/mainsms/message/send?" . $post_data);

				$send_result = json_decode($result);

			    if ($send_result->status == 'success') {
				    $sms_log->write('(Mainsms.ru) SMS send: ' . $send_result->status . ' Count: ' . $send_result->count . ' Balance: ' . $send_result->balance);
				}

				return $result;

			} else {
				$sms_log->write('(Mainsms.ru) Error: Please enter valid api_key in password and project in username field!');
			}
		}
	}

	public function prepPhone($phone) {

		$result = preg_replace('/[^0-9,]/', '', $phone);

		return $result;

	}

	protected function joinArrayValues($params) {
		$result = array();
		foreach ($params as $name => $value) {
			$result[$name] = is_array($value) ? join(',', $value) : $value;
		}
		return $result;
	}

	protected function generateSign(array $params) {
		$params['project'] = $this->username;
		ksort($params);
		return md5(sha1(join(';', array_merge($params, Array($this->password)))));
	}
}
?>