<?php
//  @copyright  OC-HELP.com
//  @website    https://oc-help.com
//  @support    support@oc-help.com
//  @developer  Alexander Vakhovskiy

final class Alphasmsua extends SmsGate {
    public function send() {
        //Sms Log
        $sms_log = new Log('sms_log.log');

        if ($this->username && $this->password) {
            $credentials = Array(
				'version'  => 'http',
				'login'    => $this->username,
				'password' => $this->password,
				'command' => 'balance',
            );

            $balance = file_get_contents('https://alphasms.ua/api/http.php?' . http_build_query($credentials));

            $balance_result = preg_replace('/[^0-9.]/', '', $balance);

            $sms_log->write('(Alphasms.ua): ' . $balance);

            if ($balance_result > 0 || $balance_result !== '0.00') {

                if ($this->to) {
                    $phone = $this->prepPhone($this->to);
                } else {
                    $phone = false;
                    $sms_log->write('(Alphasms.ua) Error: Phone destination not found!');
                }

                if ($this->from) {
                    $sender = $this->from;
                } else {
                    $sender = '';
                    $sms_log->write('(Alphasms.ua) Error: Please input Sender');
                }

                if ($balance_result && $phone) {
                    $sms = Array(
                    	'version'  => 'http',
						'login'    => $this->username,
						'password' => $this->password,
						'from'     => $sender,
						'to'       => $phone,
						'message'  => $this->message,
						'command'  => 'send'
                    );

                    $result = $this->sendSms($sms);

                    $sms_log->write('(Alphasms.ua) SMS send: ' . $result);

                    if ($this->copy) {
                        $numbers = explode(',', $this->copy);
                        foreach ($numbers as $number) {
                            if (strlen($number)<3){
                                continue;
                            }
                            $sms['to'] = $this->prepPhone($number);
                            $multi_result = $this->sendSms($sms);

                            $send_multi = json_decode($multi_result);

                            $sms_log->write('(Alphasms.ua) SMS send: ' . $send_multi);

                            sleep(1);
                        }
                    }

                    return $result;
                }
            } else {
               $sms_log->write('(Alphasms.ua):' . $balance . 'Check your Balance!');
            }

        } else {
            $sms_log->write('(Alphasms.ua) Error: Please enter valid Login or Password!');
        }
    }

    private function prepPhone($phone) {

        $result = preg_replace('/[^0-9,]/', '', $phone);

        return $result;

    }

    private function sendSms($data) {
        $result = file_get_contents("https://alphasms.ua/api/http.php?" . http_build_query($data));

        return $result;
    }

}
?>