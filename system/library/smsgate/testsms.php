<?php
//	@copyright	OC-HELP.com
//	@website	https://oc-help.com
//	@support	support@oc-help.com
// 	@developer	Alexander Vakhovskiy

final class Testsms extends SmsGate {
	public function send() {
        //Sms Log
        $sms_log = new Log('sms_log.log');

		$sms_log->write('(TEST SMS) ' . $this->message);
	}
}
?>