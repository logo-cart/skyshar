<?php
//	@copyright	OC-HELP.com
//	@website	https://oc-help.com
//	@support	support@oc-help.com
// 	@developer	Alexander Vakhovskiy

final class Smscru extends SmsGate {
	public function send() {
		//Sms Log
		$sms_log = new Log('sms_log.log');
		try {
			$client = new SoapClient('https://smsc.ru/sys/soap.php?wsdl');

			if ($this->username && $this->password) {
				$credentials = Array(
					'login' => $this->username,
					'psw' => $this->password,
				);

				$balance = $client->get_balance($credentials);

				$sms_log->write('(SMSC.ru) Balance: ' . $balance->balanceresult->balance . ' Error: ' . $balance->balanceresult->error);

				if ($this->to && $this->copy) {
					$numbers = $this->prepPhone($this->to) . ',' . $this->prepPhone($this->copy);
				} elseif ($this->to) {
					$numbers = $this->prepPhone($this->to);
				} else {
					$numbers = false;
					$sms_log->write('(SMSC.ru) Error: Phone destination not found!');
				}

				if ($this->from) {
					$sender = $this->from;
				} else {
					$sender = '';
					$sms_log->write('(SMSC.ru) Notice: Default Sender set! Please input real Sender');
				}

				if ($balance->balanceresult->balance && $numbers) {
					$sms = Array(
						'login' => $this->username,
						'psw' => $this->password,
						'phones' => $numbers,
						'mes' => $this->message,
						'sender' => $sender,
						'time' => 0,
					);

					$result = $client->send_sms($sms);

					if ($result->sendresult->cnt) {
						$sms_log->write('(SMSC.ru) SMS send: ' . $result->sendresult->cnt . ' Cost: ' . $result->sendresult->cost);
					}

					return $result;
				}

			} else {
				$sms_log->write('(SMSC.ru) Error: SMSC.ru Authentication failed!');
			}
		} catch (SoapFault $fault) {
			$sms_log->write("Ошибка SOAP: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})", E_USER_ERROR);
		}
	}

	public function prepPhone($phone) {

		$result = preg_replace('/[^0-9,]/', '', $phone);

		return $result;

	}
}
?>