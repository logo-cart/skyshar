<?php
//  @copyright  OC-HELP.com
//  @website    https://oc-help.com
//  @support    support@oc-help.com
//  @developer  Alexander Vakhovskiy

final class Rocketsms extends SmsGate {
    private $baseurl = 'https://api.rocketsms.by/json/';

    public function send() {
        //Sms Log
        $sms_log = new Log('sms_log.log');

       if ($this->username && $this->password) {
            if ($this->from) {
                $sender = $this->from;
            } else {
                $sender = 'TEST';
                $sms_log->write('(Rocketsms) Notice: Default Sender set! Please input real Sender');
            }

            $balance = $this->getBalance($this->username, $this->password);

            if($balance){
                $sms_log->write('(Rocketsms) : Balance: ' . $balance);

                if($this->to) {
                    $phone = $this->prepPhone($this->to);
                    $sms = array(
                        'username' => $this->username,
                        'password' => $this->password,
                        'phone'    => $phone,
                        'text'     => $this->message,
                        'sender'   => $sender
                    );

                    $response = $this->sendSMS($this->baseurl . 'send?' . http_build_query($sms));

                    $result = json_decode($response);

                    if($result) {
                        $sms_log->write('(Rocketsms) : ID: ' . $result->id . ' Status: '  . $result->status. ' Cost: '  . $result->cost->credits . ' Balance: ' . $result->cost->money);
                    }
                }else{
                   $sms_log->write('(Rocketsms) Error: Phone destination not found!');
                }

                if ($this->copy) {
                    $numbers = explode(',', $this->copy);
                    foreach ($numbers as $number) {
                        $phone_multi = $this->prepPhone($number);

                        $sms_multi = array(
                            'username' => $this->username,
                            'password' => $this->password,
                            'phone'    => $phone_multi,
                            'text'     => $this->message,
                            'sender'   => $sender
                        );

                        $response = $this->sendSMS($this->baseurl . 'send?' . http_build_query($sms_multi));

                        $result = json_decode($response);

                        if($result) {
                            $sms_log->write('(Rocketsms) : ID: ' . $result->id . ' Status: '  . $result->status. ' Cost: '  . $result->cost->credits . ' Balance: ' . $result->cost->money);
                        }

                    }
                }
            }else{
                $sms_log->write('(Rocketsms) : Current Balance is 0, Sms not send or Authorisation fault');
            }


        } else {
            $sms_log->write('(Rocketsms) Error: Please set correct login/password!');
        }
    }

    public function getBalance($login, $password) {
        $result = $this->sendSms($this->baseurl . 'balance?username=' . rawurlencode($login) . '&password=' . rawurlencode($password));

        return $result;
    }

    public function prepPhone($phone) {
        $result = preg_replace('/[^0-9,]/', '', $phone);
        return $result;
    }

    public function sendSms($url) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
}
?>
