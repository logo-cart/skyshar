<?php
//  @copyright  OC-HELP.com
//  @website    https://oc-help.com
//  @support    support@oc-help.com
//  @developer  Alexander Vakhovskiy

final class SmsClub extends SmsGate {
    public function send() {
        //Sms Log
        $sms_log = new Log('sms_log.log');

        if ($this->username && $this->password) {
            $credentials = Array(
				'username' => $this->username,
				'token'    => $this->password,
            );

            $balance = $this->sendSms("https://gate.smsclub.mobi/token/getbalance.php?" . http_build_query($credentials));

            $balance_result = preg_replace("/[^,.0-9]/", '', $balance);

            if ($balance_result) {
                $sms_log->write('(Smsclub) Balance: ' . $this->resultStr($balance));

                if ($this->to && $this->copy) {
                    $numbers = $this->prepPhone($this->to) . ';' . $this->prepPhone($this->copy);
                } elseif ($this->to) {
                    $numbers = $this->prepPhone($this->to);
                } else {
                    $numbers = false;
                    $sms_log->write('(Smsclub) Error: Phone destination not found!');
                }

                if ($this->from) {
                    $sender = $this->from;
                } else {
                    $sender = '';
                    $sms_log->write('(Smsclub) Notice: Default Sender set! Please input real Sender');
                }

                $message = iconv('UTF-8', 'Windows-1251', $this->message);

                if ($numbers) {
                    $sms = Array(
						'username' => $this->username,
						'token'    => $this->password,
						'from'     => $sender,
						'to'       => $numbers,
						'text'     => $message
                    );

                    $result = $this->sendSms("https://gate.smsclub.mobi/token/?" . http_build_query($sms));

                    $balance_after = $this->sendSms("https://gate.smsclub.mobi/token/getbalance.php?" . http_build_query($credentials));

                    $sms_log->write('(Smsclub) SMS send: ' .  $this->resultStr($result) . ' Balance: ' . $this->resultStr($balance_after));

                    return true;
                }
            } else {
               $sms_log->write('(Smsclub) Error: Smsclub Authentication failed!');
            }

        } else {
            $sms_log->write('(Smsclub) Error: Please enter valid api_id in login(username) field!');
        }
    }

    public function resultStr($str) {

        $result = str_replace('<br/>', ' / ', $str);

        return $result;

    }

    public function prepPhone($phone) {

        $result = preg_replace('/[^0-9,]/', '', $phone);

        return $result;

    }

    public function sendSms($url) {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
}
?>